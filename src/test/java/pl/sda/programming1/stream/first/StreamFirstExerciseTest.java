package pl.sda.programming1.stream.first;

import org.junit.jupiter.api.Test;

import pl.sda.programming1.stream.first.answer.StreamFirstAnswer;
import pl.sda.programming1.stream.first.exercise.StreamFirstExercise;

import java.util.*;

import static java.util.Collections.emptyList;
import static org.junit.jupiter.api.Assertions.assertEquals;

class StreamFirstExerciseTest {

    private StreamFirstExercise exercises = new StreamFirstExercise();

    @Test
    void test_count() {
        assertEquals(5, exercises.count(Arrays.asList(1, 2, 3, 4, 5)));
        assertEquals(0, exercises.count(Collections.emptyList()));
    }

    @Test
    void test_simpleLimit() {
        assertEquals(Arrays.asList(1, 2), exercises.simpleLimit(Arrays.asList(1, 2, 3, 4, 5)));
        assertEquals(Arrays.asList(1, 2), exercises.simpleLimit(Arrays.asList(1, 2)));
        assertEquals(Collections.emptyList(), exercises.simpleLimit(Collections.emptyList()));
    }


    @Test
    void test_limit() {
        assertEquals(Arrays.asList(1, 2, 3), exercises.limit(Arrays.asList(1, 2, 3, 4, 5), 3));
        assertEquals(Collections.emptyList(), exercises.limit(Collections.emptyList(), 0));
    }

    @Test
    void test_greaterThanMinusFive() {
        assertEquals(Arrays.asList(0, 5, 10), exercises.greaterThanMinusFive(Arrays.asList(-10, -5, 0, 5, 10)));
        assertEquals(Arrays.asList(10, 10), exercises.greaterThanMinusFive(Arrays.asList(-10, 10, -10, 10, -10)));
        assertEquals(Collections.emptyList(), exercises.greaterThanMinusFive(Collections.emptyList()));
        assertEquals(Collections.emptyList(), exercises.greaterThanMinusFive(Collections.singletonList(-20)));
    }

    @Test
    void test_numPositive() {
        assertEquals(5, exercises.numPositive(Arrays.asList(-9, 40, 2, 0, 4, -5, 98, 40, -5)));
        assertEquals(0, exercises.numPositive(emptyList()));
        assertEquals(0, exercises.numPositive(Arrays.asList(-20)));
    }

    @Test
    void test_transform() {
        assertEquals(Arrays.asList(3, 2), exercises.transform(Arrays.asList("ala", "ma")));
        assertEquals(Arrays.asList(3, 0, 2), exercises.transform(Arrays.asList("ala", "", "ma")));
        assertEquals(Arrays.asList(3, 2), exercises.transform(Arrays.asList("ala", null, "ma")));
        assertEquals(Collections.emptyList(), exercises.transform(Collections.emptyList()));
    }

    @Test
    void test_transform2() {
        assertEquals(Collections.singletonList(6), exercises.transform2(Arrays.asList("ala", "mamama", "kota", "a kot ma ale")));
        assertEquals(Arrays.asList(6, 6, 5), exercises.transform2(Arrays.asList("alaala", "mamama", "kotaa", "aaaaaaaa")));
        assertEquals(Arrays.asList(6), exercises.transform2(Arrays.asList("alaala", null, "ma")));
        assertEquals(Collections.emptyList(), exercises.transform2(Collections.emptyList()));
    }
}
