package pl.sda.programming1.stream.second;

import org.junit.jupiter.api.Test;

import pl.sda.programming1.stream.second.answer.StreamSecondAnswer;
import pl.sda.programming1.stream.second.exercise.StreamSecondExercise;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static java.util.Collections.emptyList;
import static org.junit.jupiter.api.Assertions.assertEquals;

import static pl.sda.programming1.stream.second.exercise.StreamSecondExercise.Book;

/**
 * TODO
 */
class StreamSecondExerciseTest {

    private StreamSecondExercise exercises = new StreamSecondExercise();

    private Book effectiveJava   = new Book("Effective Java", "Joshua Bloch");
    private Book finnishGrammar  = new Book("Finnish: An Essential Grammar", "Fred Karlsson");
    private Book cleanCode       = new Book("Clean Code", "Robert C. Martin");
    private Book cleanCoder      = new Book("The Clean Coder", "Robert C. Martin");
    private Book javaConcurrency = new Book("Java Concurrency in Practice", "Brian Goetz");

    private List<Book> books = Arrays.asList(effectiveJava, finnishGrammar, cleanCode, cleanCoder, javaConcurrency);

    @Test
    void test_names() {
        assertEquals(Arrays.asList("Ala", "Bob"), exercises.names(Arrays.asList("Ala", "Bob", "Bob", "Ala")));

        assertEquals(emptyList(), exercises.names(Collections.emptyList()));
    }

    @Test
    void test_sorted() {
        assertEquals(Arrays.asList("Bob", "Bob", "Ala", "Ala"), exercises.sortedNames(Arrays.asList("Ala", "Bob", "Bob", "Ala")));

        assertEquals(emptyList(), exercises.sortedNames(Collections.emptyList()));
    }

    @Test
    void test_mapping() {
        assertEquals(Arrays.asList(3,3,3,3,1), exercises.mapping(Arrays.asList("Ala", "Bob", "Bob", "Ala", "O")));

        assertEquals(emptyList(), exercises.mapping(Collections.emptyList()));
    }

    @Test
    void test_filter() {
        List<Book> expectedList = Arrays.asList(effectiveJava, finnishGrammar, cleanCoder, javaConcurrency);

        assertEquals(expectedList, exercises.filter(books));

        assertEquals(emptyList(), exercises.filter(Collections.emptyList()));
    }

    @Test
    void test_uncleBob() {
        List<Book> uncleBobBooks = Arrays.asList(cleanCode, cleanCoder);

        assertEquals(false, exercises.uncleBob(books));
        assertEquals(true, exercises.uncleBob(uncleBobBooks));


        //TODO ten test specjalnie nie przechodzi, zwróć uwage dlaczego i go popraw :)
        assertEquals(false, exercises.uncleBob(Collections.emptyList()));
    }

    @Test
    void test_uncleBob2() {
        List<Book> notUncleBobBooks = Arrays.asList(effectiveJava, javaConcurrency);

        assertEquals(true, exercises.uncleBob2(books));
        assertEquals(false, exercises.uncleBob2(notUncleBobBooks));


        //TODO ten test specjalnie nie przechodzi, zwróć uwage dlaczego i go popraw :)
        assertEquals(true, exercises.uncleBob2(Collections.emptyList()));
    }


    @Test
    void test_titlesOf() {
        assertEquals(
                Arrays.asList("Brian Goetz", "Joshua Bloch", "Robert C. Martin"),
                exercises.authorsOf(effectiveJava, cleanCode, cleanCoder, javaConcurrency));

        assertEquals(emptyList(), exercises.authorsOf());
    }
}
