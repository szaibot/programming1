package pl.sda.programming1.lambda.exercise;

import org.junit.jupiter.api.Test;

import java.util.function.Function;
import java.util.function.Predicate;

import static org.junit.jupiter.api.Assertions.*;

class LambdaExerciseTest {

    private LambdaExercise exercise = new LambdaExercise();

    @Test
    void isDivisibleBy() {
        Function<Integer, Boolean> divisibleBy = exercise.isDivisibleBy(7);

        assertTrue(divisibleBy.apply(7));
        assertTrue(divisibleBy.apply(49));
        assertFalse(divisibleBy.apply(31));
        assertTrue(divisibleBy.apply(0));
    }


    @Test
    void isTheSame() {
        String sampleString = "sample string";
        Predicate<String> test = exercise.isTheSame(sampleString);

        assertTrue(test.test(sampleString));
        assertFalse(test.test("another string"));
    }

    @Test
    void firstIsBigger() {
        assertTrue(exercise.firstIsBigger(4, 3));
        assertTrue(exercise.firstIsBigger(202, 201));
        assertTrue(exercise.firstIsBigger(0, -1));
        assertTrue(exercise.firstIsBigger(-124, -132));
    }
}
