package pl.sda.programming1.regex.answer;


import org.junit.jupiter.api.Test;

import java.util.regex.Pattern;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class RegexAnswer2 {

    /**
     * Stwórz i przetestuj regex, który będzie spełniał warunki:
     * "A. B. C." -> true
     * "A! B! C!" -> false
     *
     */
    @Test
    void test0() {
        Pattern pattern = Pattern.compile("A\\. B\\. C\\.");

        assertTrue(pattern.matcher("A. B. C.").matches());

        assertFalse(pattern.matcher("A! B! C!").matches());
    }

    /**
     * Stwórz i przetestuj regex, który będzie spełniał warunki:
     * "A? B? C?" -> true
     * "A! B! C!" -> false
     *
     */
    @Test
    void test1() {
        Pattern pattern = Pattern.compile("A\\? B\\? C\\?");

        assertTrue(pattern.matcher("A? B? C?").matches());

        assertFalse(pattern.matcher("A! B! C!").matches());
    }

    /**
     * Stwórz i przetestuj regex, który przyjmuje
     * "A\ B\ C\" -> true
     * "A! B! C!" -> false
     *
     */
    @Test
    void test2() {
        Pattern pattern = Pattern.compile("A\\\\ B\\\\ C\\\\");

        assertTrue(pattern.matcher("A\\ B\\ C\\").matches());

        assertFalse(pattern.matcher("A! B! C!").matches());
    }

    /**
     * Stwórz i przetestuj regex, który sprawdza, czy przekazany tekst:
     * składa się z jednego znaku od a do z.
     *
     */
    @Test
    void test3() {
        Pattern pattern = Pattern.compile("[a-z]");

        assertTrue(pattern.matcher("a").matches());
        assertTrue(pattern.matcher("z").matches());

        assertFalse(pattern.matcher("A").matches());
        assertFalse(pattern.matcher("Z").matches());
        assertFalse(pattern.matcher("0").matches());
        assertFalse(pattern.matcher("9").matches());
    }

    /**
     * Stwórz i przetestuj regex, który sprawdza, czy przekazany tekst:
     * składa się z jednego znaku od A do Z.
     *
     */
    @Test
    void test4() {
        Pattern pattern = Pattern.compile("[A-Z]");

        assertTrue(pattern.matcher("A").matches());
        assertTrue(pattern.matcher("Z").matches());

        assertFalse(pattern.matcher("a").matches());
        assertFalse(pattern.matcher("z").matches());
        assertFalse(pattern.matcher("0").matches());
        assertFalse(pattern.matcher("9").matches());
    }

    /**
     * Stwórz i przetestuj regex, który sprawdza, czy przekazany tekst:
     * składa się z jednego znaku od 0 do 9.
     *
     */
    @Test
    void test5() {
        Pattern pattern = Pattern.compile("[0-9]");

        assertTrue(pattern.matcher("0").matches());
        assertTrue(pattern.matcher("9").matches());

        assertFalse(pattern.matcher("a").matches());
        assertFalse(pattern.matcher("z").matches());
        assertFalse(pattern.matcher("A").matches());
        assertFalse(pattern.matcher("Z").matches());
    }

    /**
     * Stwórz i przetestuj regex, który sprawdza, czy przekazany tekst:
     * składa się z jednego znaku od 0 do 9 lub od a do Z.
     */
    @Test
    void test6() {
        Pattern pattern = Pattern.compile("[a-zA-Z0-9]");

        assertTrue(pattern.matcher("a").matches());
        assertTrue(pattern.matcher("z").matches());
        assertTrue(pattern.matcher("0").matches());
        assertTrue(pattern.matcher("9").matches());
        assertTrue(pattern.matcher("A").matches());
        assertTrue(pattern.matcher("Z").matches());

        assertFalse(pattern.matcher("aA0").matches());
        assertFalse(pattern.matcher("zZ9").matches());
        assertFalse(pattern.matcher("ab").matches());
        assertFalse(pattern.matcher("12").matches());
        assertFalse(pattern.matcher(" ").matches());
    }

    /**
     * Stwórz i przetestuj regex, który sprawdza, czy przekazany tekst:
     * składa się z jednego znaku od 0 do 9 lub od a do Z.
     * Użyj prefediniowanej klasy
     *
     */
    @Test
    void test7() {
        Pattern pattern = Pattern.compile("\\w");

        assertTrue(pattern.matcher("a").matches());
        assertTrue(pattern.matcher("z").matches());
        assertTrue(pattern.matcher("0").matches());
        assertTrue(pattern.matcher("9").matches());
        assertTrue(pattern.matcher("A").matches());
        assertTrue(pattern.matcher("Z").matches());

        assertFalse(pattern.matcher("aA0").matches());
        assertFalse(pattern.matcher("zZ9").matches());
        assertFalse(pattern.matcher("ab").matches());
        assertFalse(pattern.matcher("12").matches());
        assertFalse(pattern.matcher(" ").matches());
    }

    /**
     * Stwórz i przetestuj regex, który sprawdza, czy przekazany tekst zawiera biały znak
     * Uzyj predefiniowanej klasy
     *
     */
    @Test
    void test8() {
        Pattern pattern = Pattern.compile(".*\\s.*");

        assertTrue(pattern.matcher("ala ma kota").matches());
        assertTrue(pattern.matcher(" ala ma kota").matches());
        assertTrue(pattern.matcher(" ala ma kota ").matches());
        assertTrue(pattern.matcher("ala ma").matches());
        assertTrue(pattern.matcher(" alama").matches());
        assertTrue(pattern.matcher("alama ").matches());

        assertFalse(pattern.matcher("alamaakota").matches());
        assertFalse(pattern.matcher("").matches());
    }

    /**
     * Stwórz i przetestuj regex, który sprawdza, czy przekazany tekst nie zawiera białego znaku
     * Uzyj predefiniowanej klasy
     *
     */
    @Test
    void test9() {
        Pattern pattern = Pattern.compile("\\w*\\S\\w*");

        assertTrue(pattern.matcher("alamaakota").matches());

        assertFalse(pattern.matcher(" ").matches());
        assertFalse(pattern.matcher("ala ma kota").matches());
        assertFalse(pattern.matcher(" ala ma kota").matches());
        assertFalse(pattern.matcher(" ala ma kota ").matches());
        assertFalse(pattern.matcher("ala ma").matches());
        assertFalse(pattern.matcher(" alama").matches());
        assertFalse(pattern.matcher("alama ").matches());
    }

    /**
     * Stwórz i przetestuj regex, który sprawdza, czy przekazany tekst składa się z 3 cyfr
     */
    @Test
    void test10() {
        Pattern pattern = Pattern.compile("[0-9]{3}");

        assertTrue(pattern.matcher("000").matches());
        assertTrue(pattern.matcher("988").matches());

        assertFalse(pattern.matcher(" ").matches());
        assertFalse(pattern.matcher("   ").matches());
        assertFalse(pattern.matcher("aaa").matches());
        assertFalse(pattern.matcher("---").matches());
        assertFalse(pattern.matcher("98").matches());
        assertFalse(pattern.matcher("9811").matches());
    }

    /**
     * Stwórz i przetestuj regex, który sprawdza, czy przekazany tekst nie zawiera ani jednej cyfry.
     * "aaa"              -> true
     * "Byc albo nie byc" -> true
     * "O7 zglos sie"     -> false
     *
     */
    @Test
    void test11() {
        Pattern pattern = Pattern.compile("\\D*");

        assertTrue(pattern.matcher("aaa").matches());
        assertTrue(pattern.matcher("Byc albo nie byc").matches());

        assertFalse(pattern.matcher("0").matches());
        assertFalse(pattern.matcher("07 zglos sie").matches());
        assertFalse(pattern.matcher("zglos sie 07").matches());
        assertFalse(pattern.matcher("zglos 07 sie").matches());
    }

    /**
     * Stwórz i przetestuj regex, który sprawdza, czy przekazany tekst jest kodem pocztowym.
     * 41-230 -> true
     * 1-111  -> false
     *
     */
    @Test
    void test12() {
        Pattern pattern = Pattern.compile("\\d{2}-\\d{3}");

        assertTrue(pattern.matcher("41-230").matches());
        assertTrue(pattern.matcher("00-000").matches());

        assertFalse(pattern.matcher("1-111").matches());
        assertFalse(pattern.matcher("11-11").matches());
        assertFalse(pattern.matcher("aaa").matches());
        assertFalse(pattern.matcher("   ").matches());
        assertFalse(pattern.matcher("12345").matches());
    }

    /**
     * Stwórz i przetestuj regex, który sprawdza, czy przekazana liczba ma długosc odpowiadającą peselowi
     */
    @Test
    void test13() {
        Pattern pattern = Pattern.compile("\\d{11}");

        assertTrue(pattern.matcher("98765412345").matches());
        assertTrue(pattern.matcher("97765412345").matches());

        assertFalse(pattern.matcher("977654123456").matches());
        assertFalse(pattern.matcher("9776541234").matches());
        assertFalse(pattern.matcher("   ").matches());
    }

    /**
     * Stwórz i przetestuj regex, który sprawdza, czy przekazany tekst jest imieniem i nazwiskiem napisanym z wielkiej litery (oba czlony)
     */
    @Test
    void test14() {
        Pattern pattern = Pattern.compile("[A-Z][a-z]+ [A-Z][a-z]+");

        assertTrue(pattern.matcher("Tobiasz Kowalski").matches());

        assertFalse(pattern.matcher("Tobiasz").matches());
        assertFalse(pattern.matcher("TObiasz").matches());
        assertFalse(pattern.matcher("tobiasZ").matches());
        assertFalse(pattern.matcher("tobiaszkowalski").matches());
        assertFalse(pattern.matcher("TobiaszKowalski").matches());
        assertFalse(pattern.matcher("T K").matches());
        assertFalse(pattern.matcher("TK").matches());
    }


    /**
     * Stwórz i przetestuj regex, który sprawdza, czy przekazany tekst jest adresem email.
     * Dodatkowo podziel adres na dwie grupy identyfikator użytkownia i domenę - odpowiednio przed i po znaku @.
     * Przykład:
     *
     * kozaczek@buziaczek.pl - true
     * grupa 1: kozaczek
     * grupa 2: buziaczek.pl
     *
     */
    @Test
    void test15() {
        Pattern pattern = Pattern.compile("\\w+@\\w+\\.[a-z]+");

        assertTrue(pattern.matcher("kozaczek@buziaczek.pl").matches());
        assertTrue(pattern.matcher("bom@zlom.com").matches());

        assertFalse(pattern.matcher("kozaczek @buziaczek.pl").matches());
        assertFalse(pattern.matcher("kozaczek@ buziaczek.pl").matches());
        assertFalse(pattern.matcher("kozaczek@buziaczek .pl").matches());
        assertFalse(pattern.matcher("kozaczek@buziaczek. pl").matches());
        assertFalse(pattern.matcher("kozaczek@buzi aczek.pl").matches());
        assertFalse(pattern.matcher("koza czek@buziaczek.pl").matches());
        assertFalse(pattern.matcher("koza czek @ buzi aczek . pl").matches());
        assertFalse(pattern.matcher(" kozaczek@buziaczek.pl").matches());
        assertFalse(pattern.matcher("kozaczek@buziaczek.pl ").matches());
        assertFalse(pattern.matcher("@.").matches());
        assertFalse(pattern.matcher(" @ . ").matches());
        assertFalse(pattern.matcher(" @ . ").matches());
    }

    /**
     * Stwórz i przetestuj regex, który sprawdza, czy przekazana data jest w formacie:
     * dd/mm/yyyy
     * Aktualnie nie przejmuj sie max wartoscia dni i miesiecy, czyli
     * 35/28/9999 - 35 dzień 28 miesiąca 9999 roku jest poprawny ;)
     *
     */
    @Test
    void test16() {
        Pattern pattern = Pattern.compile("\\d{2}/\\d{2}/\\d{4}");

        assertTrue(pattern.matcher("35/28/9999").matches());
        assertTrue(pattern.matcher("12/01/2018").matches());

        assertFalse(pattern.matcher("12.01.2018").matches());
        assertFalse(pattern.matcher("2018.12.01").matches());
        assertFalse(pattern.matcher("a.b.c").matches());
    }

    /**
     * Stwórz i przetestuj regex, który sprawdza, czy przekazana dwucyfrowa liczba nie jest większa niż 31
     *
     * 15 -> true
     * 29 -> true
     * 31 -> true
     * 2 -> true (dla ułatwienia moze byc 02 na poczatku)
     *
     * 32 -> false
     * 50 -> false
     * -1 -> false
     * 0 -> false
     *
     * Nie uwzględniamy tutaj liczb ujemnych oraz 0 ;)
     */
    @Test
    void test17() {
        Pattern pattern = Pattern.compile("([1-9]|[12]\\d|3[01])");

        IntStream.rangeClosed(0, 31).forEach(value -> {
                    System.out.println(value);
                    assertTrue(pattern.matcher("" + value).matches());
                });

        assertFalse(pattern.matcher("-1").matches());
        assertFalse(pattern.matcher("32").matches());
        assertFalse(pattern.matcher("50").matches());
        assertFalse(pattern.matcher("12345").matches());

    }

    /**
     * Stwórz i przetestuj regex, który sprawdza, czy przekazany numer telefonu jest
     * dd/mm/yyyy - mm może wynosić max 12, dd - 31.
     *
     */
    @Test
    void test18() {
        String dRegex = "(0[1-9]|[12]\\d|3[01])";
        String mRegex = "(0[1-9]|[1][0-2])";
        String yRegex = "(\\d{4})";

        Pattern pattern = Pattern.compile(dRegex + "/" + mRegex + "/" + yRegex);

        assertTrue(pattern.matcher("02/01/2018").matches());
        assertTrue(pattern.matcher("12/01/2018").matches());
        assertTrue(pattern.matcher("22/01/2018").matches());
        assertTrue(pattern.matcher("31/01/2018").matches());

        assertFalse(pattern.matcher("35/28/9999").matches());
        assertFalse(pattern.matcher("32/28/9999").matches());
        assertFalse(pattern.matcher("32/01/9999").matches());
        assertFalse(pattern.matcher("31/13/9999").matches());

        assertFalse(pattern.matcher("12.01.2018").matches());
        assertFalse(pattern.matcher("12.01.2018").matches());
        assertFalse(pattern.matcher("2018.12.01").matches());
        assertFalse(pattern.matcher("a.b.c").matches());
    }
}
