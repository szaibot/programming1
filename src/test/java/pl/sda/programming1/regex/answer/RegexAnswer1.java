package pl.sda.programming1.regex.answer;


import org.junit.jupiter.api.Test;

import java.util.regex.Pattern;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class RegexAnswer1 {

    /**
     * Metoda zwraca true, jeżeli przekazany String zaczyna się od znaków "dom"
     * Zadanie w ramach rozgrzewki, skorzystaj z metod dostępnych w klasie String. Do regexów przejdziemy później ;)
     * Uruchom metode testową: test0. Jeżeli test przeszedł, rozwiązałeś zadanie poprawnie ;)
     */
    boolean testMethod0(String example) {
        return example != null && example.startsWith("dom");
    }

    @Test
    void test0() {
        assertTrue(testMethod0("dom"));
        assertTrue(testMethod0("domek"));
        assertTrue(testMethod0("domino"));

        assertFalse(testMethod0("omek"));
        assertFalse(testMethod0(""));
        assertFalse(testMethod0(null));
    }

    /**
     * Metoda zwraca true, jeżeli przekazany String zawiera znaki "mur"
     * Zadanie w ramach rozgrzewki, skorzystaj z metod dostępnych w klasie String. Do regexów przejdziemy później ;)
     * Uruchom metode testową: test1. Jeżeli test przeszedł, rozwiązałeś zadanie poprawnie ;)
     */
    boolean testMethod1(String example) {
        return example != null && example.contains("mur");
    }

    @Test
    void test1() {
        assertTrue(testMethod1("mur"));
        assertTrue(testMethod1("murowany"));
        assertTrue(testMethod1("domurowany"));
        assertTrue(testMethod1("marmur"));

        assertFalse(testMethod1("ur"));
        assertFalse(testMethod1("knur"));
        assertFalse(testMethod1(""));
        assertFalse(testMethod1(null));
    }

    /**
     * Metoda zwraca true, jeżeli przekazany String kończy się na znaki "or"
     * Zadanie w ramach rozgrzewki, skorzystaj z metod dostępnych w klasie String. Do regexów przejdziemy później ;)
     * Uruchom metode testową: test2. Jeżeli test przeszedł, rozwiązałeś zadanie poprawnie ;)
     */
    boolean testMethod2(String example) {
        return example != null && example.endsWith("or");
    }

    @Test
    void test2() {
        assertTrue(testMethod2("or"));
        assertTrue(testMethod2("murator"));

        assertFalse(testMethod2("r"));
        assertFalse(testMethod2("bo r"));
        assertFalse(testMethod2(""));
        assertFalse(testMethod2(null));
    }

    /**
     * Metoda zwraca true, jeżeli przekazany String jest taki sam jak słowo "somnambulizm"
     * Ostatnie zadanie w ramach rozgrzewki, skorzystaj z metod dostępnych w klasie String. Do regexów przejdziemy później ;)
     * Uruchom metode testową: test3. Jeżeli test przeszedł, rozwiązałeś zadanie poprawnie ;)
     */
    boolean testMethod3(String example) {
        return example != null && example.equals("somnambulizm");
    }

    @Test
    void test3() {
        assertTrue(testMethod3("somnambulizm"));

        assertFalse(testMethod3("lunatykowanie"));
        assertFalse(testMethod3(""));
        assertFalse(testMethod3(null));
    }

    /**
     * Metoda ma stworzyć regex, który sprawdza czy string zawiera znaki "mur".
     * Od tego momentu korzystamy już z klasy Pattern i Matcher.
     * W ramach uproszczenia regex i testy od tego momentu będziemy definiować w ramach jednej metody.
     * HINT użyj odpowiedniej metody na klasie Matcher
     */
    @Test
    void test4() {
        Pattern pattern = Pattern.compile("mur");

        assertTrue(pattern.matcher("mur").matches());
        assertTrue(pattern.matcher("murowany").find());
        assertTrue(pattern.matcher("domurowany").find());
        assertTrue(pattern.matcher("marmur").find());

        assertFalse(pattern.matcher("ur").find());
        assertFalse(pattern.matcher("knur").find());
        assertFalse(pattern.matcher("").find());
    }

    /**
     * Metoda ma stworzyć regex, który sprawdza czy string jest taki sam jak słowo "somnambulizm".
     * HINT użyj odpowiedniej metody na klasie Matcher
     */
    @Test
    void test5() {
        Pattern pattern = Pattern.compile("somnambulizm");

        assertTrue(pattern.matcher("somnambulizm").matches());

        assertFalse(pattern.matcher("lunatykowanie").matches());
        assertFalse(pattern.matcher("").matches());
    }

    /**
     * Stwórz i przetestuj regex, który będzie spełniał warunki:
     *  Input        -> zgodność z regexem
     * "immobilizer" -> true
     * "Immobilizer" -> false
     */
    @Test
    void test6() {
        Pattern pattern = Pattern.compile("immobilizer");

        assertTrue(pattern.matcher("immobilizer").matches());

        assertFalse(pattern.matcher("Immobilizer").matches());
    }

    /**
     * Stwórz i przetestuj regex, który sprawdza, czy przekazany tekst to:
     * "chleb", gdzie 'c' jest znakiem opcjonalnym
     */
    @Test
    void test7() {
        Pattern pattern = Pattern.compile("c?hleb");

        assertTrue(pattern.matcher("chleb").matches());
        assertTrue(pattern.matcher("hleb").matches());

        assertFalse(pattern.matcher("bleb").matches());
        assertFalse(pattern.matcher("cleb").matches());

    }

    /**
     * Stwórz i przetestuj regex, który sprawdza, czy przekazany tekst to:
     * "brum", gdzie 'u' może wystąpić od 0 do nieskonczonej ilości razy
     */
    @Test
    void test8() {
        Pattern pattern = Pattern.compile("bru*m");

        assertTrue(pattern.matcher("brm").matches());
        assertTrue(pattern.matcher("brum").matches());
        assertTrue(pattern.matcher("bruuuuuuuuuuuuum").matches());

        assertFalse(pattern.matcher("bm").matches());
        assertFalse(pattern.matcher("bum").matches());
    }

    /**
     * Stwórz i przetestuj regex, który sprawdza, czy przekazany tekst to:
     * "brum", gdzie 'u' może wystąpić od 1 do nieskonczonej ilości razy
     */
    @Test
    void test9() {
        Pattern pattern = Pattern.compile("bru+m");

        assertTrue(pattern.matcher("brum").matches());
        assertTrue(pattern.matcher("bruuuuuuuuuuuuum").matches());

        assertFalse(pattern.matcher("brm").matches());
        assertFalse(pattern.matcher("bm").matches());
        assertFalse(pattern.matcher("bum").matches());
    }

    /**
     * Stwórz i przetestuj regex, który sprawdza, czy przekazany tekst to:
     * "brum", gdzie 'u' może wystąpić od 2 do 7 razy
     */
    @Test
    void test10() {
        Pattern pattern = Pattern.compile("bru{2,7}m");

        assertTrue(pattern.matcher("bruum").matches());
        assertTrue(pattern.matcher("bruuuum").matches());
        assertTrue(pattern.matcher("bruuuuuuum").matches());

        assertFalse(pattern.matcher("bruuuuuuuuum").matches());
        assertFalse(pattern.matcher("brm").matches());
        assertFalse(pattern.matcher("brum").matches());
    }

    /**
     * Stwórz i przetestuj regex, który sprawdza, czy przekazany tekst to:
     * "brum", gdzie 'u' może wystąpić od 0 do 3 razy
     */
    @Test
    void test11() {
        Pattern pattern = Pattern.compile("bru{0,3}m");

        assertTrue(pattern.matcher("brm").matches());
        assertTrue(pattern.matcher("bruum").matches());
        assertTrue(pattern.matcher("bruuum").matches());

        assertFalse(pattern.matcher("bruuuum").matches());
    }

    /**
     * Stwórz i przetestuj regex, który sprawdza, czy przekazany tekst to:
     * "brum", gdzie 'u' może wystąpić od 3 do nieskonczonej ilości razy
     */
    @Test
    void test12() {
        Pattern pattern = Pattern.compile("bru{3,}m");

        assertTrue(pattern.matcher("bruuum").matches());
        assertTrue(pattern.matcher("bruuuum").matches());

        assertFalse(pattern.matcher("brm").matches());
        assertFalse(pattern.matcher("bruum").matches());
    }


    /**
     * Stwórz i przetestuj regex, który będzie spełniał warunki:
     * "la mamba"       -> true
     * "lala mamba"     -> true
     * "lalala mamba"   -> true
     * "lalalala mamba" -> true
     *
     * "mamba"          -> false
     * "ba mamba"       -> false
     */
    @Test
    void test13() {
        Pattern pattern = Pattern.compile("(la)+ mamba");

        assertTrue(pattern.matcher("la mamba").matches());
        assertTrue(pattern.matcher("lala mamba").matches());
        assertTrue(pattern.matcher("lalala mamba").matches());

        assertFalse(pattern.matcher("mamba").matches());
        assertFalse(pattern.matcher("ba mamba").matches());
    }


    /**
     * Stwórz i przetestuj regex, który będzie spełniał warunki:
     * "rum" -> true
     * "ram" -> true
     * "rom" -> true
     * "rem" -> true
     *
     * "brum"       -> false
     * "rumbumbum"  -> false
     * "rm"         -> false
     */
    @Test
    void test14() {
        Pattern pattern = Pattern.compile("r.m");

        assertTrue(pattern.matcher("rum").matches());
        assertTrue(pattern.matcher("ram").matches());
        assertTrue(pattern.matcher("rom").matches());
        assertTrue(pattern.matcher("rem").matches());
        assertTrue(pattern.matcher("r m").matches());

        assertFalse(pattern.matcher("brum").matches());
        assertFalse(pattern.matcher("rumbumbum").matches());
        assertFalse(pattern.matcher("rm").matches());
    }


    /**
     * Przygotuj przypadki testowe dla podanego regex'u
     */
    @Test
    void test15() {
        Pattern pattern = Pattern.compile("mi*.*ka");

        assertTrue(pattern.matcher("mika").matches());
        assertTrue(pattern.matcher("mekka").matches());
        assertTrue(pattern.matcher("milka").matches());
        assertTrue(pattern.matcher("miiiiiiiilka").matches());
        assertTrue(pattern.matcher("miiiiiiiiaduadausdnausdka").matches());

        assertFalse(pattern.matcher("amika").matches());
        assertFalse(pattern.matcher("amiko").matches());
        assertFalse(pattern.matcher("amioa").matches());
    }

    /**
     * Przygotuj regex dla podanych przypadków testowych
     */
    @Test
    void test16() {
        Pattern pattern = Pattern.compile("a+.ma");

        assertTrue(pattern.matcher("a ma").matches());
        assertTrue(pattern.matcher("alma").matches());
        assertTrue(pattern.matcher("aaaaaalma").matches());
        assertTrue(pattern.matcher("aaaaaarma").matches());

        assertFalse(pattern.matcher("lma").matches());
        assertFalse(pattern.matcher("a").matches());
        assertFalse(pattern.matcher("blma").matches());
        assertFalse(pattern.matcher("clna").matches());
    }

    /**
     * Stwórz i przetestuj regex, który sprawdza, czy przekazany tekst:
     * zaczyna się od: "Prezes"
     */
    @Test
    void test17() {
        Pattern pattern = Pattern.compile("^Prezes.*");

        assertTrue(pattern.matcher("Prezes ma kota").matches());

        assertFalse(pattern.matcher("  Prezes  ").matches());
        assertFalse(pattern.matcher("Ucho Prezesa").matches());
    }

    /**
     * Stwórz i przetestuj regex, który sprawdza, czy przekazany tekst:
     * kończy się na: "Zgierza"
     */
    @Test
    void test18() {
        Pattern pattern = Pattern.compile(".*Zgierza$");

        assertTrue(pattern.matcher("Male miasto kolo Zgierza").matches());

        assertFalse(pattern.matcher("  Zgierza  ").matches());
        assertFalse(pattern.matcher("Zgierza nie ma").matches());

    }

}
