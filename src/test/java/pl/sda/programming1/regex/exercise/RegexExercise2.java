package pl.sda.programming1.regex.exercise;


import org.junit.jupiter.api.Test;

class RegexExercise2 {

    /**
     * Stwórz i przetestuj regex, który będzie spełniał warunki:
     * "A. B. C." -> true
     * "A! B! C!" -> false
     *
     */
    @Test
    void test0() {

    }

    /**
     * Stwórz i przetestuj regex, który będzie spełniał warunki:
     * "A? B? C?" -> true
     * "A! B! C!" -> false
     *
     */
    @Test
    void test1() {

    }

    /**
     * Stwórz i przetestuj regex, który przyjmuje
     * "A\ B\ C\  -> true
     * "A! B! C!" -> false
     *
     */
    @Test
    void test2() {

    }

    /**
     * Stwórz i przetestuj regex, który sprawdza, czy przekazany tekst:
     * składa się z jednego znaku od a do z.
     *
     */
    @Test
    void test3() {

    }

    /**
     * Stwórz i przetestuj regex, który sprawdza, czy przekazany tekst:
     * składa się z jednego znaku od A do Z.
     *
     */
    @Test
    void test4() {

    }

    /**
     * Stwórz i przetestuj regex, który sprawdza, czy przekazany tekst:
     * składa się z jednego znaku od 0 do 9.
     *
     */
    @Test
    void test5() {

    }

    /**
     * Stwórz i przetestuj regex, który sprawdza, czy przekazany tekst:
     * składa się z jednego znaku od 0 do 9 lub od a do Z.
     *
     */
    @Test
    void test6() {

    }

    /**
     * Stwórz i przetestuj regex, który sprawdza, czy przekazany tekst:
     * składa się z jednego znaku od 0 do 9 lub od a do Z.
     *
     */
    @Test
    void test7() {

    }

    /**
     * Stwórz i przetestuj regex, który sprawdza, czy przekazany tekst zawiera biały znak
     * Uzyj predefiniowanej klasy
     *
     */
    @Test
    void test8() {

    }

    /**
     * Stwórz i przetestuj regex, który sprawdza, czy przekazany tekst nie zawiera białego znaku
     * Uzyj predefiniowanej klasy
     *
     */
    @Test
    void test9() {

    }

    /**
     * Stwórz i przetestuj regex, który sprawdza, czy przekazany tekst składa się z 3 cyfr
     */
    @Test
    void test10() {

    }

    /**
     * Stwórz i przetestuj regex, który sprawdza, czy przekazany tekst nie zawiera ani jednej cyfry.
     * "aaa"              -> true
     * "Byc albo nie byc" -> true
     * "O7 zglos sie"     -> false
     *
     */
    @Test
    void test11() {

    }

    /**
     * Stwórz i przetestuj regex, który sprawdza, czy przekazany tekst jest kodem pocztowym.
     * 41-230 -> true
     * 1-111  -> false
     *
     */
    @Test
    void test12() {

    }

    /**
     * Stwórz i przetestuj regex, który sprawdza, czy przekazana liczba ma długosc odpowiadającą peselowi
     */
    @Test
    void test13() {

    }

    /**
     * Stwórz i przetestuj regex, który sprawdza, czy przekazany tekst jest imieniem i nazwiskiem napisanym z wielkiej litery (oba czlony)
     */
    @Test
    void test14() {

    }


    /**
     * Stwórz i przetestuj regex, który sprawdza, czy przekazany tekst jest adresem email.
     * Dodatkowo podziel adres na dwie grupy identyfikator użytkownia i domenę - odpowiednio przed i po znaku @.
     * Przykład:
     *
     * kozaczek@buziaczek.pl - true
     * grupa 1: kozaczek
     * grupa 2: buziaczek.pl
     *
     */
    @Test
    void test15() {

    }

    /**
     * Stwórz i przetestuj regex, który sprawdza, czy przekazana data jest w formacie:
     * dd/mm/yyyy
     * Aktualnie nie przejmuj sie max wartoscia dni i miesiecy, czyli
     * 35.28.9999 - 35 dzień 28 miesiąca 9999 roku jest poprawny ;)
     *
     */
    @Test
    void test16() {

    }

    /**
     * Stwórz i przetestuj regex, który sprawdza, czy przekazana dwucyfrowa liczba nie jest większa niż 31
     *
     * 15 -> true
     * 29 -> true
     * 31 -> true
     * 2 -> true (dla ułatwienia moze byc 02 na poczatku)
     *
     * 32 -> false
     * 50 -> false
     * -1 -> false
     * 0 -> false
     *
     * Nie uwzględniamy tutaj liczb ujemnych oraz 0 ;)
     */
    @Test
    void test17() {

    }

    /**
     * Stwórz i przetestuj regex, który sprawdza, czy przekazana data jest w formacie
     * dd/mm/yyyy - mm może wynosić max 12, dd - 31.
     *
     */
    @Test
    void test18() {

    }
}
