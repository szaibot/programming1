package pl.sda.programming1.regex.exercise;


import org.junit.jupiter.api.Test;

import java.util.regex.Pattern;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class RegexExercise1 {

    /**
     * Metoda zwraca true, jeżeli przekazany String zaczyna się od znaków "dom"
     * Zadanie w ramach rozgrzewki, skorzystaj z metod dostępnych w klasie String. Do regexów przejdziemy później ;)
     * Uruchom metode testową: test0. Jeżeli test przeszedł, rozwiązałeś zadanie poprawnie ;)
     */
    boolean testMethod0(String example) {
        return false;
    }

    @Test
    void test0() {
       assertTrue(testMethod0("dom"));
       assertTrue(testMethod0("domek"));
       assertTrue(testMethod0("domino"));

       assertFalse(testMethod0("omek"));
       assertFalse(testMethod0(""));
       assertFalse(testMethod0(null));
    }

    /**
     * Metoda zwraca true, jeżeli przekazany String zawiera znaki "mur"
     * Zadanie w ramach rozgrzewki, skorzystaj z metod dostępnych w klasie String. Do regexów przejdziemy później ;)
     * Uruchom metode testową: test1. Jeżeli test przeszedł, rozwiązałeś zadanie poprawnie ;)
     */
    boolean testMethod1(String example) {
        return false;
    }

    @Test
    void test1() {
        assertTrue(testMethod1("mur"));
        assertTrue(testMethod1("murowany"));
        assertTrue(testMethod1("domurowany"));
        assertTrue(testMethod1("marmur"));

        assertFalse(testMethod1("ur"));
        assertFalse(testMethod1("knur"));
        assertFalse(testMethod1(""));
        assertFalse(testMethod1(null));
    }

    /**
     * Metoda zwraca true, jeżeli przekazany String kończy się na znaki "or"
     * Zadanie w ramach rozgrzewki, skorzystaj z metod dostępnych w klasie String. Do regexów przejdziemy później ;)
     * Uruchom metode testową: test2. Jeżeli test przeszedł, rozwiązałeś zadanie poprawnie ;)
     */
    boolean testMethod2(String example) {
        return false;
    }

    @Test
    void test2() {
        assertTrue(testMethod2("or"));
        assertTrue(testMethod2("murator"));

        assertFalse(testMethod2("r"));
        assertFalse(testMethod2("bo r"));
        assertFalse(testMethod2(""));
        assertFalse(testMethod2(null));
    }

    /**
     * Metoda zwraca true, jeżeli przekazany String jest taki sam jak słowo "somnambulizm"
     * Ostatnie zadanie w ramach rozgrzewki, skorzystaj z metod dostępnych w klasie String. Do regexów przejdziemy później ;)
     * Uruchom metode testową: test3. Jeżeli test przeszedł, rozwiązałeś zadanie poprawnie ;)
     */
    boolean testMethod3(String example) {
        return false;
    }

    @Test
    void test3() {
        assertTrue(testMethod3("somnambulizm"));

        assertFalse(testMethod3("lunatykowanie"));
        assertFalse(testMethod3(""));
        assertFalse(testMethod3(null));
    }

    /**
     * Metoda ma stworzyć regex, który sprawdza czy string zawiera znaki "mur".
     * Od tego momentu korzystamy już z klasy Pattern i Matcher.
     * W ramach uproszczenia regex i testy od tego momentu będziemy definiować w ramach jednej metody.
     * HINT użyj odpowiedniej metody na klasie Matcher
     */
    @Test
    void test4() {

    }

    /**
     * Metoda ma stworzyć regex, który sprawdza czy string jest taki sam jak słowo "somnambulizm".
     * HINT użyj odpowiedniej metody na klasie Matcher
     */
    @Test
    void test5() {
    }

    /**
     * Stwórz i przetestuj regex, który będzie spełniał warunki:
     *  Input        -> zgodność z regexem
     * "immobilizer" -> true
     * "Immobilizer" -> false
     */
    @Test
    void test6() {
    }

    /**
     * Stwórz i przetestuj regex, który sprawdza, czy przekazany tekst to:
     * "chleb", gdzie 'c' jest znakiem opcjonalnym
     */
    @Test
    void test7() {
    }

    /**
     * Stwórz i przetestuj regex, który sprawdza, czy przekazany tekst to:
     * "brum", gdzie 'u' może wystąpić od 0 do nieskonczonej ilości razy
     */
    @Test
    void test8() {
    }

    /**
     * Stwórz i przetestuj regex, który sprawdza, czy przekazany tekst to:
     * "brum", gdzie 'u' może wystąpić od 1 do nieskonczonej ilości razy
     */
    @Test
    void test9() {
    }

    /**
     * Stwórz i przetestuj regex, który sprawdza, czy przekazany tekst to:
     * "brum", gdzie 'u' może wystąpić od 2 do 7 razy
     */
    @Test
    void test10() {
    }

    /**
     * Stwórz i przetestuj regex, który sprawdza, czy przekazany tekst to:
     * "brum", gdzie 'u' może wystąpić od 0 do 3 razy
     */
    @Test
    void test11() {
    }

    /**
     * Stwórz i przetestuj regex, który sprawdza, czy przekazany tekst to:
     * "brum", gdzie 'u' może wystąpić od 3 do nieskonczonej ilości razy
     */
    @Test
    void test12() {
    }


    /**
     * Stwórz i przetestuj regex, który będzie spełniał warunki:
     * "la mamba"       -> true
     * "lala mamba"     -> true
     * "lalala mamba"   -> true
     * "lalalala mamba" -> true
     *
     * "mamba"          -> false
     * "ba mamba"       -> false
     */
    @Test
    void test13() {
    }


    /**
     * Stwórz i przetestuj regex, który będzie spełniał warunki:
     * "rum" -> true
     * "ram" -> true
     * "rom" -> true
     * "rem" -> true
     *
     * "brum"       -> false
     * "rumbumbum"  -> false
     * "rm"         -> false
     */
    @Test
    void test14() {
    }


    /**
     * Przygotuj przypadki testowe dla podanego regex'u
     */
    @Test
    void test15() {
        Pattern pattern = Pattern.compile("mi*.*ka");

    }

    /**
     * Przygotuj regex dla podanych przypadków testowych
     */
    @Test
    void test16() {
        Pattern pattern = Pattern.compile(""); //uzupelnij regex

        assertTrue(pattern.matcher("a ma").matches());
        assertTrue(pattern.matcher("alma").matches());
        assertTrue(pattern.matcher("aaaaaalma").matches());
        assertTrue(pattern.matcher("aaaaaarma").matches());

        assertFalse(pattern.matcher("lma").matches());
        assertFalse(pattern.matcher("a").matches());
        assertFalse(pattern.matcher("blma").matches());
        assertFalse(pattern.matcher("clna").matches());
    }

    /**
     * Stwórz i przetestuj regex, który sprawdza, czy przekazany tekst:
     * zaczyna się od: "Prezes"
     */
    @Test
    void test17() {
    }

    /**
     * Stwórz i przetestuj regex, który sprawdza, czy przekazany tekst:
     * kończy się na: "Zgierzu"
     */
    @Test
    void test18() {
    }

}
