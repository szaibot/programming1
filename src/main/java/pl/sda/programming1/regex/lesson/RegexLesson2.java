package pl.sda.programming1.regex.lesson;


import java.util.regex.Matcher;
import java.util.regex.Pattern;

class RegexLesson2 {

    public static void main(String[] args) {
        //Problem a co jak chcemy, żeby w naszym wyrażeniu mogła wystąpić kropka? Czyli znak specjalny, albo znak zapytania?

        //Znak specjalny jeżeli ma być traktowany normalnie musi być poprzedzony znakiem '\'.
        //Tutaj natomiast wchodzi ograniczenie klasy String w Javie.
        print("wartosci znakow specjalnych", RegexLesson2::extractSpecialCharacter);

        //TODO ROZWIAZ ZESTAW ZADAN: RegexExercise2 0-2

        //Problem: chciałbym móc wpisać zakres elementów. Np od a do z albo do 0 do 9.

        //[a-z] itd. - klasy
        print("Klasy", RegexLesson2::classes);

        //TODO ROZWIAZ ZESTAW ZADAN: RegexExercise2 3-7

        //Problem: no ale zawsze muszę powtarzać te znaki, a jak się pomyle?

        // /d, /w, /s - Predefiniowane klasy
        print("Predefiniowane klasy", RegexLesson2::predefinedClasses);

        //TODO ROZWIAZ ZESTAW ZADAN: RegexExercise2 8-16

        //| - alternatywa
        print("Alternatywa", RegexLesson2::alternative);

        //Problem: a jakbym chciał mieć kilka sekcji w ramach jednego regexa?

        //() - Grupy
        print("Grupy", RegexLesson2::groups);

        //TODO ROZWIAZ ZESTAW ZADAN: RegexExercise2 17-18
    }

    private static void extractSpecialCharacter() {
        //"Pierwsze zdanie\. Drugie zdanie\." -> "Pierwsze zdanie. Drugie zdanie."
        //"Pierwsze zdanie. Drugie zdanie." -> "Pierwsze zdanie! Drugie zdanie?", "Pierwsze zdanie1 Drugie zdanie2"

        Pattern wrongPattern = Pattern.compile("Pierwsze zdanie. Drugie zdanie.");

        print(wrongPattern.matcher("Pierwsze zdanie. Drugie zdanie.").matches());
        print(wrongPattern.matcher("Pierwsze zdanie? Drugie zdanie!").matches());
        print(wrongPattern.matcher("Pierwsze zdanie1 Drugie zdanie2").matches());

        Pattern workingPattern = Pattern.compile("Pierwsze zdanie\\. Drugie zdanie\\.");

        print(workingPattern.matcher("Pierwsze zdanie. Drugie zdanie.").matches());
        System.out.println();

        print(workingPattern.matcher("Pierwsze zdanie? Drugie zdanie?").matches());
        System.out.println();

        //"haslo: *****"
        System.out.println("Przyklad z haslem");
        Pattern anotherPattern = Pattern.compile("haslo: \\*\\*\\*\\*\\*\\*\\*");
        print(anotherPattern.matcher("haslo: *******").matches());
        System.out.println();

        print(anotherPattern.matcher("haslo: password").matches());
    }

    private static void classes() {
        // "[abcd]1" -> "a1", "b1", "c1", "d1"  <=> "e1", "ae1", "bc1"
        // "[a-d]1" -> "a1", "b1", "c1", "d1"  ale już nie "e1", "ae1", "bc1"
        Pattern p = Pattern.compile("[abcd]1");
        Pattern p2 = Pattern.compile("[a-d]1");

        print(p.matcher("a1").matches());
        print(p.matcher("b1").matches());
        print(p.matcher("c1").matches());
        print(p.matcher("d1").matches());

        print(p.matcher("e1").matches());
        print(p.matcher("ae1").matches());
        print(p.matcher("bc1").matches());

        // "[a-z]1" -> "a1", "b1", "y1", "z1" ale już nie "az1", "A1", "Z1"
        Pattern p3 = Pattern.compile("[a-z]1");

        print(p3.matcher("a1").matches());
        print(p3.matcher("b1").matches());
        print(p3.matcher("y1").matches());
        print(p3.matcher("z1").matches());

        print(p3.matcher("az1").matches());
        print(p3.matcher("A1").matches());
        print(p3.matcher("Z1").matches());


        // "[A-Z]1" -> "A1", "Z1" ale już nie  "a1", "z1"
        Pattern p4 = Pattern.compile("[A-Z]1");

        print(p4.matcher("A1").matches());
        print(p4.matcher("Z1").matches());

        print(p4.matcher("a1").matches());
        print(p4.matcher("z1").matches());

        // "a[0-9]" -> "a0", "a9", ale już nie ""
        Pattern p5 = Pattern.compile("a[0-9]");

        print(p5.matcher("a0").matches());
        print(p5.matcher("a9").matches());

        print(p5.matcher("a").matches());
        print(p5.matcher("").matches());

        Pattern p5a = Pattern.compile("[a-zA-Z0-9]");

        print(p5a.matcher("a").matches());
        print(p5a.matcher("z").matches());
        print(p5a.matcher("A").matches());
        print(p5a.matcher("Z").matches());
        print(p5a.matcher("0").matches());
        print(p5a.matcher("9").matches());

        print(p5a.matcher("aA0").matches());
        print(p5a.matcher("lL5").matches());
        print(p5a.matcher("zZ9").matches());

        // "[a-zA-Z]1" -> "a1", "z1", "A1", "Z1" ale już nie "aA1", "zZ1", "aZ1"
        Pattern p6 = Pattern.compile("[a-zA-Z]1");

        print(p6.matcher("a1").matches());
        print(p6.matcher("A1").matches());
        print(p6.matcher("z1").matches());
        print(p6.matcher("Z1").matches());

        print(p6.matcher("aA1").matches());
        print(p6.matcher("zZ1").matches());

        // "[^0-9]" -> "b",  "c", "A", "B"
        Pattern p7 = Pattern.compile("[^0-9]");

        print(p7.matcher("a").matches());
        print(p7.matcher("z").matches());
        print(p7.matcher("A").matches());
        print(p7.matcher("Z").matches());
        print(p7.matcher(" ").matches());

        print(p7.matcher("0").matches());
        print(p7.matcher("7").matches());
        print(p7.matcher("9").matches());
    }

    private static void predefinedClasses() {
        // "\d" -> dowolna cyfra [0-9]
        Pattern p1 = Pattern.compile("[0-9]");
        Pattern p2 = Pattern.compile("\\d");

        // "\D" -> dowolny znak, który nie jest cyfrą [^0-9]
        Pattern p3 = Pattern.compile("[^0-9]");
        Pattern p4 = Pattern.compile("\\D");

        // "\w" -> cyfra, litera lub _ [a-zA-Z0-9_]
        Pattern p5 = Pattern.compile("[a-zA-Z0-9_]");
        Pattern p6 = Pattern.compile("\\w");

        // "\W" -> nie cyfra, litera lub _ [^a-zA-Z0-9_]
        Pattern p7 = Pattern.compile("[^a-zA-Z0-9_]");
        Pattern p8 = Pattern.compile("\\W");

        // "\s" -> biale znaki (spacja, tab, nowa linia) [ \t\n\r\f\x0B]
        Pattern p9 = Pattern.compile("[ \\t\\n\\r\\f\\x0B]");
        Pattern p10 = Pattern.compile("\\s");

        // "\S" -> nie biale znaki (spacja, tab, nowa linia) [ \t\n\r\f\x0B]
        Pattern p11 = Pattern.compile("[^ \\t\\n\\r\\f\\x0B]");
        Pattern p12 = Pattern.compile("\\S");
    }

    private static void alternative() {
        Pattern p = Pattern.compile("pierwszy|drugi");

        System.out.println(p.matcher("pierwszy").matches());
        System.out.println(p.matcher("drugi").matches());


        System.out.println(p.matcher("|").matches());
        System.out.println(p.matcher("pierwszy|drugi").matches());
        System.out.println(p.matcher("trzeci").matches());
    }

    private static void groups() {
        Pattern p = Pattern.compile("([0-9]{3})-([0-9]{3})");

        Matcher matcher = p.matcher("293-430");
        System.out.println(matcher.matches());
        System.out.println(matcher.group(0));
        System.out.println(matcher.group(1));
        System.out.println(matcher.group(2));
    }

    private static void print(boolean s) {
        System.out.println(s);
    }

    private static void print(String name, Runnable method) {
        System.out.println("*******************");
        System.out.println(name);
        method.run();
        System.out.println("*******************");
        System.out.println();
    }
}
