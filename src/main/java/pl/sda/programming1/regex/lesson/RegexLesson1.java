package pl.sda.programming1.regex.lesson;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

class RegexLesson1 {

    public static void main(String[] args) {
        //Wytlumaczyc po co nam regex

        //podstawowe klasy do regexow
        print("Basic classes", RegexLesson1::basicClasses);

        //find vs matches
        print("Find vs Matches", RegexLesson1::findVsMatches);

        //wielkosc ma znaczenie
        print("Case sensitive", RegexLesson1::caseSensitive);

        //TODO ROZWIAZ ZESTAW ZADAN: RegexExercise1 0-6

        //pokazać ulatwienie w intelliJ

        //znak ? - (O - 1)
        print("Znak ?", RegexLesson1::questionMark);

        //znak * - (O - many)
        print("Znak *", RegexLesson1::starMark);

        //problem: a jakbym chciał na pewno mieć jeden element?

        //znak + - (O - many)
        print("Znak +", RegexLesson1::plusMark);

        //znak . - dowolny znak
        print("Dowolny znak", RegexLesson1::anyCharacter);

        //TODO ROZWIAZ ZESTAW ZADAN: RegexExercise1 7-9

        //Problem: a jakbym chciał od 3 do 5, albo jakiś inny nieregularny sposób?

        //{x,y} znak poprzedzający może wystąpić od x do y razy.
        print("Niereguralna ilosc", RegexLesson1::specificCount);

        //Problem: To tylko pojedyncze znaki

        //() zwiększa zakres
        print("Powtorzenie dluzszego stringa", RegexLesson1::repeatLongerString);

        //Te cuda można łączyć
        print("Laczenie znakow specjalnych", RegexLesson1::multiplyMarks);

        //^ oznacza początek łańcucha znaków
        print("Zakotwicz na poczatku", RegexLesson1::startsWith);

        //$ oznacza koniec łańcucha znaków
        print("Zakotwicz na koncu", RegexLesson1::endsWith);

        //TODO trzeci zestaw zadań EXECISE 10-18
    }


    private static void basicClasses() {

        Pattern compiledPattern = Pattern.compile("ma");

        Matcher matcher = compiledPattern.matcher("Ala ma kota");

        boolean found = matcher.find();
        print(found);
    }

    private static void findVsMatches() {
        Pattern compiledPattern = Pattern.compile("Tobiasz");
        Matcher matcher = compiledPattern.matcher("Tobiasz Kowalski");

        boolean found = matcher.find();
        System.out.println(found);

        boolean matches = matcher.matches();
        System.out.println(matches);
    }

    private static void caseSensitive() {
        Pattern compiledPattern = Pattern.compile("tobiasz");
        Matcher matcher = compiledPattern.matcher("Tobiasz");

        System.out.println(matcher.find());
        System.out.println(matcher.matches());
    }

    private static void questionMark() {
        //? znak poprzedzający jest opcjonalny (0 do 1).
        //"br?ak" -> "brak", "bak" <-> "back", "blak"
        Pattern p = Pattern.compile("br?ak");

        print(p.matcher("brak").matches());
        print(p.matcher("bak").matches());
        System.out.println();

        print(p.matcher("back").matches());
        print(p.matcher("blak").matches());
    }

    private static void starMark() {
        //* znak poprzedzający może wystąpić wiele razy (0 do wiele).
        //"zi*mno" -> "zmno", "zimno", "ziiiimno" <-> "iiiiiimno"
        Pattern p = Pattern.compile("zi*mno");

        print(p.matcher("zmno").matches());
        print(p.matcher("zimno").matches());
        print(p.matcher("ziiiiiimno").matches());
        System.out.println();

        print(p.matcher("iiiiiimno").matches());
    }

    private static void plusMark() {
        //+ znak poprzedzający musi wystąpić raz lub więcej razy (1 do wiele).
        //"zi+mno" -> "zimno", "ziiiimno" <-> "zmno"
        Pattern p = Pattern.compile("zi+mno");

        print(p.matcher("zimno").matches());
        print(p.matcher("ziiiiiimno").matches());
        System.out.println();

        print(p.matcher("zmno").matches());
    }

    private static void specificCount() {
        //"zi{3,5}mno" -> "ziiiimno", "ziiiiimno", "ziiimno"
        //"zi{2,}mno" -> "ziimno", "ziiiiiiimno"
        //"zi{0,2}mno" -> "zmno", "zimno", "ziimno"

        Pattern p1 = Pattern.compile("zi{3,5}mno");

        print(p1.matcher("ziiimno").matches());
        print(p1.matcher("ziiiimno").matches());
        print(p1.matcher("ziiiiimno").matches());
        System.out.println();

        print(p1.matcher("zimno").matches());
        print(p1.matcher("ziiiiiiiiiimno").matches());
        System.out.println();

        //******************************************************************
        Pattern p2 = Pattern.compile("zi{2,}mno");

        print(p2.matcher("ziimno").matches());
        print(p2.matcher("ziiiimno").matches());
        print(p2.matcher("ziiiiiiiiiiiiiiiiiiiiiiiiiiiiimno").matches());
        System.out.println();

        print(p2.matcher("zmno").matches());
        print(p2.matcher("zimno").matches());
        System.out.println();

        //******************************************************************
        Pattern p3 = Pattern.compile("zi{0,2}mno");

        print(p3.matcher("zmno").matches());
        print(p3.matcher("zimno").matches());
        print(p3.matcher("ziimno").matches());
        System.out.println();

        print(p3.matcher("ziiimno").matches());
        print(p3.matcher("ziiiiiiiiiimno").matches());
        System.out.println();
    }


    private static void anyCharacter() {
        //"b.k" -> "bak", "bok", "bek", "bik", "b9k", "b k"
        Pattern p = Pattern.compile("b.k");

        print(p.matcher("bak").matches());
        print(p.matcher("bok").matches());
        print(p.matcher("bek").matches());
        print(p.matcher("bik").matches());
        print(p.matcher("b9k").matches());
        print(p.matcher("b k").matches());
        System.out.println();

        print(p.matcher("bzik").matches());
    }

    private static void repeatLongerString() {
        //"(ja)+ chcialem" -> "jajajajaja chcialem"
        Pattern p = Pattern.compile("(ja)+ chcialem");

        print(p.matcher("ja chcialem").matches());
        print(p.matcher("jajajajajaja chcialem").matches());
    }

    private static void multiplyMarks() {
        //k+a.*ta -> "kata", "katapulta", "karta", "kasia ma kota", "kkkka#$*&JHDFStatata" <->
        Pattern p = Pattern.compile("k+a.*ta");

        print(p.matcher("kata").matches());
        print(p.matcher("katapulta").matches());
        print(p.matcher("karta").matches());
        print(p.matcher("kasia ma kota").matches());
        print(p.matcher("kkkka#$*&JHDFStatata").matches());
    }

    private static void startsWith() {
        Pattern p = Pattern.compile("^ala");

        print(p.matcher("ala ma kota").find());

        print(p.matcher("bo ala ma kota").find());
    }

    private static void endsWith() {
        Pattern p = Pattern.compile("kota$");

        print(p.matcher("ala ma kota").find());

        print(p.matcher("ala ma kota i psa").find());
    }

    private static void print(boolean s) {
        System.out.println(s);
    }

    private static void print(String name, Runnable method) {
        System.out.println("*******************");
        System.out.println(name);
        method.run();
        System.out.println("*******************");
        System.out.println();
    }
}
