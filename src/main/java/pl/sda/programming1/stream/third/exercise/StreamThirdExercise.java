package pl.sda.programming1.stream.third.exercise;

import pl.sda.programming1.stream.third.answer.StreamThirdAnswer;
import pl.sda.programming1.stream.third.lesson.StreamLesson3;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Exercises related with {@link StreamLesson3} lesson.
 * Answers can be found in {@link StreamThirdAnswer}
 *
 */
public class StreamThirdExercise {

    /**
     * Przerób metodę, żeby zwracała List<String> zamiast List<List<String>>.

        W klasie {@link StreamThirdAnswer} poprawiona metoda zmieniła nazwię na {@link StreamThirdAnswer#method0New}.
     */
    public List<List<String>> method0(List<List<String>> lists) {
        return lists.stream()
                .collect(Collectors.toList());
    }

    /**
     * Metoda łączy listy Integerów w jedną.
     * Wejście -> [{1,2,3}
     *             {4,5,6}]
     * Wyjście -> [1,2,3,4,5,6]
     *
     */
    public List<Integer> method1(List<List<Integer>> lists) {
        throw new UnsupportedOperationException();
    }

    /**
     * Metoda przerabia listę zdań, na listę słów.
     * Wejście -> ["Ala ma kota"
     *             "Tomek ma psa"]
     * Wyjście -> ["Ala", "ma", "kota", "Tomek", "ma", "psa"]
     *
     */
    public List<String> method2(List<String> lists) {
        throw new UnsupportedOperationException();
    }

    /**
     * Metoda przerabia listę autorów, na listę artykułów wszystkich autorów
     * Wejście -> [{"Sienkiewicz" ["header1", "header2"]},
     *             {"Mickiewicz" ["header3", "header4"]}]
     * Wyjście -> ["header1", "header2", "header3", "header4"]
     *
     */
    public List<StreamLesson3.Author.Article> method3(List<StreamLesson3.Author> authors) {
        throw new UnsupportedOperationException();
    }

    /**
     * Metoda przerabia listę autorów, na listę artykułów względem nazwiska autora.
     *
     * Wejście -> [{"Sienkiewicz" ["header1", "header2"]},
     *             {"Mickiewicz" ["header3", "header4"]}] as List<Author>
     *
     * Wyjście -> [
     *              ["Sienkiewicz",  ["header1", "header2"]],
     *              ["Mickiewicz",   ["header3", "header4"]]
     *
     *            ] as Map<String, Set<Author.Article>>
     *
     */
    public Map<String, Set<StreamLesson3.Author.Article>> method5(List<StreamLesson3.Author> authors) {
        throw new UnsupportedOperationException();
    }

    /**
     * Metoda łączy przekazane string w jeden. Znakiem łączącym będzie '-',
     * natomiast prefix i suffix zostaną przekazane przez parametry
     *
     * Wejście -> "Ala ma kota", "<", "~"
     * Wyjście -> "<Ala-ma-kota~"
     *
     */
    public String method6(List<String> strings, String prefix, String suffix) {
        throw new UnsupportedOperationException();
    }

    /**
     * Metoda grupuje po długości stringa. Pamiętaj o nie liczeniu pustych znaków na początku i końcu!
     *
     * Wejście -> ["first", "second", "  third   "]
     * Wyjscie -> [
     *              {5, ["first, "  third   "]},
     *              {6, ["second"]}
     *            ]
     *
     */
    public Map<Integer, List<String>> method7(List<String> strings) {
        throw new UnsupportedOperationException();
    }

    /**
     * Metoda zlicza ilość słów
     *
     * Wejście -> ["first", "second", "first"]
     * Wyjscie -> [
     *              {"first", 2},
     *              {"second", 1}
     *            ]
     *
     */
    public Map<String, Long> method8(List<String> strings) {
        throw new UnsupportedOperationException();
    }

    /**
     * Metoda sumuje wagi owoców.
     *
     * Wejście -> [
     *              {"banana", 1}
     *              {"banana", 3}
     *              {"apple", 2}
     *            ]
     * Wyjscie -> [
     *              {"banana", 4},
     *              {"apple", 2}
     *            ]
     *
     */
    public Map<String, Integer> method9(List<StreamLesson3.Fruit> fruits) {
        throw new UnsupportedOperationException();
    }


    /**
     * Metoda łączy przekazane stringi w zdanie
     */
    public String method10(List<String> strings) {
        throw new UnsupportedOperationException();
    }

}
