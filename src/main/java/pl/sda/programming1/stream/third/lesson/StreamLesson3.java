package pl.sda.programming1.stream.third.lesson;


import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * TODO
 */
public class StreamLesson3 {

    private static final List<List<Integer>> lists = Arrays.asList(
            Arrays.asList(1, 2, 3),
            Arrays.asList(4, 5, 6),
            Arrays.asList(7, 8, 9)
    );

    private static final List<Author> authors = Arrays.asList(

            new Author("FirstName", new HashSet<>(Arrays.asList(
                    new Author.Book("a"),
                    new Author.Book("b")
            ))),

            new Author("SecondName", new HashSet<>(Arrays.asList(
                    new Author.Book("c"),
                    new Author.Book("d")
            )))
    );

    private static final List<Fruit> fruits = Arrays.asList(
            new Fruit("banana", 3),
            new Fruit("orange", 1),
            new Fruit("apple", 5),
            new Fruit("orange", 2),
            new Fruit("banana", 4)

    );

    public static void main(String[] args) {
        //------------------------------------------------------------- flatMap
        //-----------------------simple map
            List<Stream<Integer>> collect = lists.stream()
                .map(list -> list.stream())
                .collect(Collectors.toList());

            List<List<Integer>> collect1 = lists.stream()
                .map(list -> list.stream().collect(Collectors.toList()))
                .collect(Collectors.toList());

            print("Simple map" + collect1);

        //-----------------------flatmap List<List<>>
        //explain with books on the floor
        
            List<Integer> collect2 = lists.stream()
                    .flatMap(list -> list.stream())
                    .collect(Collectors.toList());
    
            print("Flatmap" + collect2);
            
            /*
                [[1,2,3], [4,5,6], [7,8,9]]
    
                List<List<Integer>>  -> stream                          -> Stream<List<Integer>>
                Stream<List<Integer> -> flatmap(list -> list.stream())  -> Stream<Integer>
                Stream<Integer>      -> collect(toList())               -> List<Integer>
    
                [1,2,3,4,5,6,7,8,9]
            */

            /*
                Podsumowanie

                Stream<String[]>     -> flatmap ->  Stream<String>
                Stream<List<String>> -> flatmap ->  Stream<String>
                Stream<Set<String>>  -> flatmap ->  Stream<String>
                Stream<List<Floor>>  -> flatmap ->  Stream<Floor>

                [[1,2], [3,4], [5,6]]  -> flatmap -> [1,2,3,4,5,6]
                [['a','b'], ['c','d']] -> flatmap -> ['a','b','c','d']
            */

        //-----------------------flatmap List<NotList<>>

            List<Author.Book> books = authors.stream()
                .flatMap(author -> author.books.stream())
                .collect(Collectors.toList());

            print("Books flatmap:"+ books);


        //------------------------------------------------------------- collector#toSet
            Set<Author> authorSet = authors.stream()
                .collect(Collectors.toSet());

            print("toSet="+authorSet);

        //------------------------------------------------------------- collector#toMap
            Map<String, Author> authorMap = authors.stream()
                .collect(Collectors.toMap(
                        author -> author.getName(),
                        author -> author
                ));

            print("to map="+authorMap);

        //------------------------------------------------------------- collector#joining
            String alaMaKota = Arrays.asList("ala", "ma", "kota").stream()
                .collect(Collectors.joining(" ", "[", "]"));

            print("Joining: "+alaMaKota);

        //------------------------------------------------------------- collector#groupingBy
            Map<Integer, List<String>> grouping = Arrays.asList("first", "second", "third")
                .stream()
                .collect(Collectors.groupingBy(string -> string.length()));

            print("Grouping: " + grouping);


        //------------------------------------------------------------- collector#counting
        //Describe Function.identity()
            Map<String, Long> counting = Arrays.asList("first", "second", "third", "first", "second", "first")
                .stream()
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

            print("Counting " + counting);

        //------------------------------------------------------------- collector#summing
            Map<String, Integer> summing = fruits.stream()
                .collect(Collectors.groupingBy(
                        fruit -> fruit.getName(),
                        Collectors.summingInt(fruit -> fruit.getWeight())
                ));

            print("Summing " + summing);


        //------------------------------------------------------------- collector#reduce
            String reduce = Arrays.asList("ala", "ma", "kota").stream()
                .reduce("", (first, second) -> first + " " + second);

            print("Reduce " + reduce);

            Optional<String> optionalReduce = Arrays.asList("ala", "ma", "kota").stream()
                .reduce((first, second) -> first + " " + second);

            print("Reducing " + optionalReduce.orElse(""));


        //------------------------------------------------------------- map split
            List<String> sentences = Arrays.asList("ala ma kota", "kot ma ale");

            List<String> stringSplit = sentences.stream()
                .map(s -> s.split("//s"))
                .flatMap(array -> Stream.of(array))
                .collect(Collectors.toList());

            print("String split" + stringSplit);

            /*
                 ['ala ma kota','kot ma ale']

                List<String>     -> stream                              -> Stream<String>
                Stream<String>   -> map(s -> s.split("//s"))            -> Stream<String[]>
                Stream<String[]> -> flatmap(array -> Stream.of(array))  -> Stream<String>
                Stream<String>   -> collect(toList())                   -> List<String>

                ['ala', 'ma', 'kota','kot', 'ma', 'ale']
            */
    }

    private static void print(String value) {
        System.out.println("*******************");
        System.out.println(value);
        System.out.println("*******************");
        System.out.println();
    }

    public static class Author {
        String name;
        Set<Author.Book> books;
        Set<Author.Article> articles;

        public Author(String name, Set<Author.Book> books) {
            this.name = name;
            this.books = books;
        }

        public Author(String name, Set<Author.Book> books, Set<Author.Article> articles) {
            this.name = name;
            this.books = books;
            this.articles = articles;
        }

        public static class Book {
            String title;

            public Book(String title) {
                this.title = title;
            }

            @Override
            public String toString() {
                return "Book{" +
                        "title='" + title + '\'' +
                        '}';
            }
        }

        public static class Article {
            String header;

            public Article(String header) {
                this.header = header;
            }
        }

        public String getName() {
            return name;
        }

        public Set<Author.Book> getBooks() {
            return books;
        }

        public Set<Author.Article> getArticles() {
            return articles;
        }

        @Override
        public String toString() {
            return "Author{" +
                    "name='" + name + '\'' +
                    ", books=" + books +
                    ", articles=" + articles +
                    '}';
        }
    }

    public static class Fruit {
        String name;
        int weight;

        public Fruit(String name, int weight) {
            this.name = name;
            this.weight = weight;
        }

        public String getName() {
            return name;
        }

        public int getWeight() {
            return weight;
        }
    }
}
