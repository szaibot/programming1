package pl.sda.programming1.stream.third.answer;

import pl.sda.programming1.stream.third.exercise.StreamThirdExercise;
import pl.sda.programming1.stream.third.lesson.StreamLesson3;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamThirdAnswer extends StreamThirdExercise {


    /**
     * answer for {@link StreamThirdExercise#method0(List)}
     */
    public List<String> method0New(List<List<String>> lists) {
        return lists.stream()
                .flatMap(list ->  list.stream())
                .collect(Collectors.toList());
    }

    @Override
    public List<Integer> method1(List<List<Integer>> lists) {
        return lists.stream()
                .flatMap(list -> list.stream())
                .collect(Collectors.toList());
    }

    @Override
    public List<String> method2(List<String> lists) {
        return lists.stream()
                .flatMap(string -> Stream.of(string.split("\\s")))
                .collect(Collectors.toList());
    }

    @Override
    public List<StreamLesson3.Author.Article> method3(List<StreamLesson3.Author> authors) {
        return authors.stream()
                .flatMap(author -> author.getArticles().stream())
                .collect(Collectors.toList());
    }

    @Override
    public Map<String, Set<StreamLesson3.Author.Article>> method5(List<StreamLesson3.Author> authors) {
                return authors.stream()
                        .collect(Collectors.toMap(
                                author -> author.getName(),
                                author -> author.getArticles())
                        );
    }

    @Override
    public String method6(List<String> strings, String prefix, String suffix) {
        return strings.stream()
                .collect(Collectors.joining("-", prefix,  suffix));
    }

    @Override
    public Map<Integer, List<String>> method7(List<String> strings) {
        return strings.stream()
                .collect(Collectors.groupingBy(string -> string.trim().length()));
    }

    @Override
    public Map<String, Long> method8(List<String> strings) {
        return strings.stream()
                .collect(Collectors.groupingBy(
                            Function.identity(),
                            Collectors.counting())
                );
    }

    @Override
    public Map<String, Integer> method9(List<StreamLesson3.Fruit> fruits) {
        return fruits.stream()
                .collect(Collectors.groupingBy(
                        fruit -> fruit.getName(),
                        Collectors.summingInt(fruit -> fruit.getWeight())
                ));
    }

    @Override
    public String method10(List<String> strings) {
        return strings.stream()
                .reduce((first, second) -> first + " " + second)
                .orElse("");
    }
}
