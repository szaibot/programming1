package pl.sda.programming1.stream.second.lesson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * TODO
 */
class StreamLesson2 {

    public static void main(String[] args) {
        //------------------------------------------------------------- distinct
            List<String> list = Arrays.asList("a", "a", "a");

            List<String> distinct = list.stream()
                .distinct()
                .collect(Collectors.toList());

            print("Distinct="+distinct);

            long distinctCount = list.stream().distinct().count();
            print("Distinct count="+distinctCount);


        //------------------------------------------------------------- sort
            List<String> names = Arrays.asList("Bob", "Tom", "Anna");

            List<String> sorted = names.stream()
                .sorted()
                .collect(Collectors.toList());


            print("Sorted before"+names);
            print("Sorted after"+sorted);

        List<String> reverseSorted = names.stream()
                .sorted(Comparator.reverseOrder())
                .collect(Collectors.toList());

            print("Sorted before"+names);
            print("Reverse sort"+reverseSorted);


        //------------------------------------------------------------- forEach
            names.forEach(name -> System.out.println(name));

        //------------------------------------------------------------- allMatch
            boolean allMatch = names.stream()
                .allMatch(name -> name.length() > 2);

            print("All names longer than 2? " + allMatch);

            boolean notAllMatch = names.stream()
                .allMatch(name -> name.length() > 3);

            print("All names longer than 3? " + notAllMatch);

        //------------------------------------------------------------- anyMatch

            boolean anyMatch = names.stream()
                .anyMatch(name -> name.length() > 3);

            print("Any name longer than 3? " + anyMatch);

    }

    private static void print(String value) {
        System.out.println("*******************");
        System.out.println(value);
        System.out.println("*******************");
        System.out.println();
    }

}
