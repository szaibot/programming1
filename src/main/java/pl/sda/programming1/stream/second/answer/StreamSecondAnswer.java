package pl.sda.programming1.stream.second.answer;

import pl.sda.programming1.stream.second.exercise.StreamSecondExercise;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

/**
 * TODO
 */
public class StreamSecondAnswer extends StreamSecondExercise {

    @Override
    public List<String> names(List<String> names) {
        return names.stream()
                .distinct()
                .collect(Collectors.toList());
    }

    @Override
    public List<String> sortedNames(List<String> names) {
        return names.stream()
                .sorted(Comparator.reverseOrder())
                .collect(Collectors.toList());
    }

    @Override
    public List<Integer> mapping(List<String> names) {
        List<Integer> integers = new ArrayList<>();
        names.forEach(name -> integers.add(name.length()));

        return integers;
    }

    @Override
    public List<Book> filter(List<Book> books) {
        return books.stream()
                .filter(book -> book.getTitle().length() > 10)
                .collect(Collectors.toList());
    }

    @Override
    public boolean uncleBob(List<Book> books) {
        return books.stream()
                .allMatch(book -> book.getAuthor().equals("Robert C. Martin"));
    }

    @Override
    public boolean uncleBob2(List<Book> books) {
        return books.stream()
                .anyMatch(book -> book.getAuthor().equals("Robert C. Martin"));
    }

    @Override
    public List<String> authorsOf(Book... books) {
        return Arrays.stream(books)
                .map(Book::getAuthor)
                .sorted()
                .distinct()
                .collect(toList());
    }
}
