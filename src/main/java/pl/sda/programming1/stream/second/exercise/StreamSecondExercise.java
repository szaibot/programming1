package pl.sda.programming1.stream.second.exercise;

import java.util.List;

public class StreamSecondExercise {

    /**
     * Metoda zwraca listę imion bez duplikatów
     */
    public List<String> names(List<String> names) {
        throw new UnsupportedOperationException("NOT IMPLEMENTED!!");
    }

    /**
     * Metoda zwraca odwrotnie posortowaną listę imion:
     * Przykład:
     * Wejściowe -> "Ala", "Bob"
     * Wyjściowe -> "Bob", "Ala"
     */
    public List<String> sortedNames(List<String> names) {
        throw new UnsupportedOperationException("NOT IMPLEMENTED!!");
    }

    /**
     * Metoda zwraca listę długości stringów przekazanych jako parametr.
     * Dodatkowym wymaganiem jest użycie metody forEach.
     * HINT: trzeba stworzyć listę pomocniczą
     */
    public List<Integer> mapping(List<String> names) {
        throw new UnsupportedOperationException("NOT IMPLEMENTED!!");
    }

    /**
     * Metoda zwraca listę książek. Klasa Book zdefiniowana poniżej.
     * Zwracane są tylko takie książki, których tytuł jest dłuższy niż 10 znaków
     */
    public List<Book> filter(List<Book> books) {
        throw new UnsupportedOperationException("NOT IMPLEMENTED!!");
    }

    /**
     * Metoda zwraca true, jeżeli wszystkie przekazane książki są autorstwa "Robert C. Martin" (dokładnie takie imię i nazwisko)
     */
    public boolean uncleBob(List<Book> books) {
        throw new UnsupportedOperationException("NOT IMPLEMENTED!!");
    }

    /**
     * Metoda zwraca true, jeżeli choć jedna przekazana książka jest autorstwa "Robert C. Martin" (dokładnie takie imię i nazwisko)
     */
    public boolean uncleBob2(List<Book> books) {
        throw new UnsupportedOperationException("NOT IMPLEMENTED!!");
    }

    /**
     * Metoda zwraca listę autorów książek znajdujących się na podanej liście.
     * Wynikowa lista jest posortowana alfabetycznie oraz nie zawiera duplikatów.
     */
    public List<String> authorsOf(Book... books) {
        throw new UnsupportedOperationException("NOT IMPLEMENTED!!");
    }

    public static class Book {

        private final String title;
        private final String author;

        public Book(String title, String author) {
            this.title = title;
            this.author = author;
        }

        public String getTitle() {
            return title;
        }

        public String getAuthor() {
            return author;
        }
    }

}
