package pl.sda.programming1.stream.first.exercise;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * TODO
 */
public class StreamFirstExercise {

    /**
     * Metoda zlicza ilość liczb w podanej liście. Użyj streamu nie metody size() ;)
     */
    public long count(List<Integer> numbers) {
        throw new UnsupportedOperationException("NOT IMPLEMENTED!!");
    }

    /**
     * Metoda zwraca listę elementów zmniejszoną do 2
     */
    public List<Integer> simpleLimit(List<Integer> numbers) {
        throw new UnsupportedOperationException("NOT IMPLEMENTED!!");
    }

    /**
     * Metoda zwraca listę zmniejszoną do ilości elementów przekazanych przez parametr.
     */
    public List<Integer> limit(List<Integer> numbers, int limit) {
        throw new UnsupportedOperationException("NOT IMPLEMENTED!!");
    }

    /**
     * Metoda zwraca liczby większe od -5
     */
    public List<Integer> greaterThanMinusFive(List<Integer> numbers) {
        throw new UnsupportedOperationException("NOT IMPLEMENTED!!");
    }

    /**
     * Metoda zwraca ilość liczb dodatnich w podanej liście.
     */
    public long numPositive(List<Integer> numbers) {
        throw new UnsupportedOperationException("NOT IMPLEMENTED!!");
    }

    /**
     * Metoda zamienia liste stringów, na liste intów odpowiadających długości znaków.
     * Przykład
     * wejściowe -> ["ala", "ma"]
     * wyjściowe -> [3, 2]
     */
    public List<Integer> transform(List<String> numbers) {
        throw new UnsupportedOperationException("NOT IMPLEMENTED!!");
    }

    /**
     * Metoda zamienia liste stringów, na liste intów odpowiadających długości znaków.
     * Dodatkowo ogranicza ilość elementów w zwracanej liście do 3
     * i zwraca tylko takie elementy, które są większe niż 4
     * Przykład
     * wejściowe -> ["ala", "mamama", "kota", "a kot ma ale"]
     * wyjściowe -> [6]
     */
    public List<Integer> transform2(List<String> numbers) {
        throw new UnsupportedOperationException("NOT IMPLEMENTED!!");
    }

}
