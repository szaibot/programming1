package pl.sda.programming1.stream.first.lesson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

class StreamLesson1 {

    public static void main(String[] args) {
        List<String> names = Arrays.asList("Ala", "Tomek", "Krzysztof", "Monika", "Ola");


        //------------------------------------------------------------- count
        //Zlicz obiekty
        //-----------------------------zwykly for
            int count = 0;
            for (String name : names) {
                count ++;
            }

            print("Ilosc elementow=" + count);

        //-----------------------------stream
        //imperative vs declarative
            long count1 = names.stream().count();

            print("Ilosc elementow(stream)=" + count1);


        //-------------------------------------------------------------- limit
        //ogranicz ilość elementów do 3
        //----------------------------- for
            int limit = 0;
            List<String> limitedNames = new ArrayList<>();
            for (String name : names) {
                if (limit < 3) {
                    limitedNames.add(name);
                }

                limit ++;
            }

            print("Ograniczona ilość elementów=" + limitedNames);

        //----------------------------- stream
            List<String> limitedNamesStream = names.stream()
                    .limit(3)
                    .collect(Collectors.toList());

            print("Ograniczona ilość elementów(stream)=" + limitedNamesStream);

        //-------------------------------------------------------------- filter
        //tylko imiona nie dłuższe niż 4 znaki
        //----------------------------- for
            List<String> filteredNames = new ArrayList<>();
            for (String name : names) {
                if (name.length() <= 4) {
                    filteredNames.add(name);
                }
            }

            print("Imiona nie dluzsze niz 4=" + filteredNames);

        //----------------------------- stream
        //jaki parametr przyjmuje metoda filter?
        //----------------klasa anonimowa
            Predicate<String> predicate = new Predicate<String>() {
                @Override
                public boolean test(String s) {
                    return s.length() <= 4;
                }
            };

            names.stream()
                    .filter(predicate)
                    .collect(Collectors.toList());

        //----------------lambda poza streamem
            Predicate<String> predicateLambda = s -> s.length() <= 4;

            names.stream()
                    .filter(predicateLambda)
                    .collect(Collectors.toList());

        //----------------lambda inline w streamie
            List<String> filteredNamesStream = names.stream()
                    .filter(name -> name.length() <= 4)
                    .collect(Collectors.toList());

            print("Imiona nie dłuższe niż 4(stream)=" + filteredNamesStream);

        //------------------------------------------------------------- map
        //pokazać typ zwracany z metody map
        //----------------------------- for
            List<Integer> lengths = new ArrayList<>();
            for (String name : names) {
                lengths.add(name.length());
            }

            print("Dlugosci imion=" + lengths);


        //-----------------------------stream
            List<Integer> lengthsStream = names.stream()
                .map(name -> name.length())
                .collect(Collectors.toList());

            print("Dlugosci imion(stream)=" + lengthsStream);

        //-------------------------------------------------------------- lazy stream

            System.out.println("*******************");
            System.out.println("before stream creation" + names);
            Stream<String> stream = names.stream();
            System.out.println("before stream filtration" + names);
            Stream<String> stringStream = stream.filter(name -> name.length() <= 4);
            System.out.println("before stream finalization" + names);
            List<String> collect = stringStream.collect(Collectors.toList());
            System.out.println("after stream finalization(old collection)" + names);
            System.out.println("after stream finalization(new collection)" + collect);
            System.out.println("*******************");
            System.out.println();


        //-------------------------------------------------------------- single execution
            Stream<String> partialStream = names.stream()
                    .filter(name -> name.length() <= 4);

            List<String> firstCollection = partialStream.collect(Collectors.toList());

            //java.lang.IllegalStateException: stream has already been operated upon or closed
            //List<String> secondCollection = partialStream.collect(Collectors.toList());

        //-------------------------------------------------------------- inne sposoby tworzenia
            Arrays.stream(new String[]{"a", "b", "c"});

            IntStream.of(1,2,3,4,5);
            IntStream.range(1,10);

            new Random().doubles();
            new Random().ints();

            Stream.empty();

            Pattern.compile("\\s").splitAsStream("Ala ma kota");
    }

    private static void print(String value) {
        System.out.println("*******************");
        System.out.println(value);
        System.out.println("*******************");
        System.out.println();
    }
}
