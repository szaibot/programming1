package pl.sda.programming1.stream.first.answer;

import pl.sda.programming1.stream.first.exercise.StreamFirstExercise;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.*;

/**
 * TODO
 */
public class StreamFirstAnswer extends StreamFirstExercise {

    @Override
    public long count(List<Integer> numbers) {
        return numbers.stream().count();
    }

    @Override
    public List<Integer> simpleLimit(List<Integer> numbers) {
        return numbers.stream()
                .limit(2)
                .collect(Collectors.toList());
    }

    @Override
    public List<Integer> limit(List<Integer> numbers, int limit) {
        return numbers.stream()
                .limit(limit)
                .collect(Collectors.toList());
    }

    @Override
    public List<Integer> greaterThanMinusFive(List<Integer> numbers) {
        return numbers.stream()
                .filter(integer -> integer > -5)
                .collect(Collectors.toList());
    }

    @Override
    public long numPositive(List<Integer> numbers) {
        return numbers.stream().filter(x -> x > 0).count();
    }

    @Override
    public List<Integer> transform(List<String> numbers) {
        return numbers.stream()
                .filter(Objects::nonNull)
                .map(String::length)
                .collect(Collectors.toList());
    }

    @Override
    public List<Integer> transform2(List<String> numbers) {
        return numbers.stream()
                .filter(Objects::nonNull)
                .map(String::length)
                .limit(3)
                .filter(value -> value > 4)
                .collect(Collectors.toList());
    }
}
