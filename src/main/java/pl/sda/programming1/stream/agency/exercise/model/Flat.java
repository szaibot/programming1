package pl.sda.programming1.stream.agency.exercise.model;

public class Flat {
    private final int flatNumber;
    private final int roomsCount;
    private final String owner;
    private final boolean withPool;
    private final long price;

    public Flat(int flatNumber, int roomsCount, String owner, boolean withPool, int price) {
        this.flatNumber = flatNumber;
        this.roomsCount = roomsCount;
        this.owner = owner;
        this.withPool = withPool;
        this.price = price;
    }

    public int getFlatNumber() {
        return flatNumber;
    }

    public int getRoomsCount() {
        return roomsCount;
    }

    public String getOwner() {
        return owner;
    }

    public boolean isWithPool() {
        return withPool;
    }

    public long getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return "Flat{" +
                "flatNumber=" + flatNumber +
                ", roomsCount=" + roomsCount +
                ", owner='" + owner + '\'' +
                ", withPool=" + withPool +
                ", price=" + price +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Flat)) return false;

        Flat flat = (Flat) o;

        if (flatNumber != flat.flatNumber) return false;
        if (roomsCount != flat.roomsCount) return false;
        if (withPool != flat.withPool) return false;
        if (price != flat.price) return false;
        return owner != null ? owner.equals(flat.owner) : flat.owner == null;
    }

    @Override
    public int hashCode() {
        int result = flatNumber;
        result = 31 * result + roomsCount;
        result = 31 * result + (owner != null ? owner.hashCode() : 0);
        result = 31 * result + (withPool ? 1 : 0);
        result = 31 * result + (int) (price ^ (price >>> 32));
        return result;
    }
}
