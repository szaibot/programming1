package pl.sda.programming1.stream.agency.exercise;

import pl.sda.programming1.stream.agency.exercise.model.Block;
import pl.sda.programming1.stream.agency.exercise.model.Flat;
import pl.sda.programming1.stream.agency.exercise.model.Floor;
import pl.sda.programming1.stream.agency.exercise.model.Material;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface AgencyRepository {

    String getAreaName();

    String getAreaOwner();

    List<Block> getBlocks();

    long buildingsCount();

    long getGlassBlocks();

    List<Block> getConcreteBlocks();

    List<Block> getBlocks(Material material);

    List<Floor> getFloors();

    long getFloorsCount();

    List<Floor> getFloorsWithPatio();

    List<Floor> getAboveSecondFloor();

    List<Floor> getFloors(int floorNumber);

    List<Flat> getFlats();

    List<Flat> getFlatsWithPool();

    List<String> getFlatsOwners();

    long getNumberOfFlats();

    long getNumberOfFlats(int minimumRoomNumber);

    Map<Integer, Material> blockNumberToMaterial();

    Map<Material, Integer> blockMaterialToFloorCount();

    Map<Integer, Boolean> floorNumberToPatio();

    Map<Block, List<Floor>> blockToFloors();

    Set<Flat> tenFlats();

    String owners();

    String owners2();

    String ownersPromo();

    String ownersWithPrefixAndSuffix(String delimiter, String prefix, String suffix);

    Map<Material, List<Block>> groupByMaterial();

    Map<Integer, List<Block>> groupByFloorNumber();

    Map<Boolean, List<Floor>> groupByPatio();

    Map<Boolean, List<Flat>> groupByPool();

    Map<Boolean, List<Floor>> groupFloorsByPool();

    Map<Material, Long> countByMaterial();

    Map<Integer, Long> countByFloorNumber();

    Map<Boolean, Long> countByPatio();

    Map<Integer, Long> countByFlatNumber();

    Map<Boolean, Long> countByPool();

    Map<Integer, Long> countByRoomsNumber();

    Map<Boolean, Long> groupFlatsByPool();

    Map<Material, Integer> sumBlockNumbers();

    Map<Integer, Integer> sumFlatCountOnFloor();

    Map<Boolean, Integer> sumFlatCountWithPatio();

    Map<String, Long> sumPriceWithPool();
}
