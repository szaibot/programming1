package pl.sda.programming1.stream.agency.exercise.model;

public enum Material {
    GLASS,
    CONCRETE,
    STEEL
}
