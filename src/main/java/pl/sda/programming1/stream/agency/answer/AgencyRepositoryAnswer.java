package pl.sda.programming1.stream.agency.answer;

import pl.sda.programming1.stream.agency.exercise.AgencyRepositoryExercise;
import pl.sda.programming1.stream.agency.exercise.model.Block;
import pl.sda.programming1.stream.agency.exercise.model.Flat;
import pl.sda.programming1.stream.agency.exercise.model.Floor;
import pl.sda.programming1.stream.agency.exercise.model.Material;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import static pl.sda.programming1.stream.agency.exercise.model.Material.CONCRETE;
import static pl.sda.programming1.stream.agency.exercise.model.Material.GLASS;

public class AgencyRepositoryAnswer extends AgencyRepositoryExercise {

    @Override
    public String getAreaName() {
        return area.getName();
    }

    @Override
    public String getAreaOwner() {
        return area.getOwnerName();
    }

    @Override
    public List<Block> getBlocks() {
        return area.getBlocks();
    }

    /**
     * Metoda zwraca ilość budynków na osiedlu
     */
    @Override
    public long buildingsCount() {
        return area.getBlocks().stream()
                .count();
    }

    //THEORY PART 1


    /**
     * Metoda zwraca ilość budynków ze szkła na osiedlu
     * Przerób rozwiązanie na streama
     */
    @Override
    public long getGlassBlocks() {
        return area.getBlocks().stream()
                .filter(block -> block.getMaterial() == GLASS)
                .count();
    }

    /**
     * Metoda zwraca wszystkie budynki z betonu
     * Zaimplementuj najpierw z użyciem pętli for a potem streama
     */
    @Override
    public  List<Block> getConcreteBlocks() {
        return area.getBlocks().stream()
                .filter(block -> block.getMaterial() == CONCRETE)
                .collect(Collectors.toList());
    }

    /**
     * Metoda zwraca wszystkie budynki z materiału przekazanego jako parametr
     */
    @Override
    public List<Block> getBlocks(Material material) {
        return area.getBlocks().stream()
                .filter(block -> block.getMaterial() == material)
                .collect(Collectors.toList());
    }


    //THEORY PART 2
    //---------------------------------------------------------------------flatMap


    /**
     * Metoda zwraca wszystkie piętra we wszystkich budynkach
     * Zaimplementuj najpierw z użyciem pętli for a potem streama
     */
    @Override
    public List<Floor> getFloors() {
        return area.getBlocks().stream()
                .flatMap(block -> block.getFloors().stream())
                .collect(Collectors.toList());
    }

    /**
     * Metoda zwraca ilość wszystkich pięter
     */
    @Override
    public long getFloorsCount() {
        return area.getBlocks().stream()
                .flatMap(block -> block.getFloors().stream())
                .count();
    }

    /**
     * Metoda zwraca piętra, które mają patio
     */
    @Override
    public List<Floor> getFloorsWithPatio() {
        return area.getBlocks().stream()
                .flatMap(block -> block.getFloors().stream())
                .filter(floor -> floor.isWithPatio())
                .collect(Collectors.toList());
    }

    /**
     * Metoda zwraca piętra, które są powyżej 2 poziomu
     * (piętra numerowane są od 1)
     */
    @Override
    public List<Floor> getAboveSecondFloor() {
        return area.getBlocks().stream()
                .flatMap(block -> block.getFloors().stream())
                .filter(floor -> floor.getFloorNumber() > 2)
                .collect(Collectors.toList());
    }


    /**
     * Metoda zwraca piętra, które są z poziomu przekazanego jako parametr.
     */
    @Override
    public List<Floor> getFloors(int floorNumber) {
        return area.getBlocks().stream()
                .flatMap(block -> block.getFloors().stream())
                .filter(floor -> floor.getFloorNumber() == floorNumber)
                .collect(Collectors.toList());
    }


    /**
     * Metoda zwraca wszystkie mieszkania na wszystkich piętrach we wszystkich blokach
     */
    @Override
    public List<Flat> getFlats() {
        return area.getBlocks().stream()
                .flatMap(block -> block.getFloors().stream())
                .flatMap(floor -> floor.getFlats().stream())
                .collect(Collectors.toList());
    }

    /**
     * Metoda zwraca wszystkie mieszkania z basenem
     */
    @Override
    public List<Flat> getFlatsWithPool() {
        return area.getBlocks().stream()
                .flatMap(block -> block.getFloors().stream())
                .flatMap(floor -> floor.getFlats().stream())
                .filter(flat -> flat.isWithPool())
                .collect(Collectors.toList());
    }

    /**
     * Metoda zwraca nazwiska właścicieli wszystkich mieszkań na osiedlu
     */
    @Override
    public List<String> getFlatsOwners() {
        return area.getBlocks().stream()
                .flatMap(block -> block.getFloors().stream())
                .flatMap(floor -> floor.getFlats().stream())
                .map(flat -> flat.getOwner())
                .collect(Collectors.toList());
    }

    /**
     * Metoda zwraca ilość mieszkań na całym osiedlu
     */
    @Override
    public long getNumberOfFlats() {
        return area.getBlocks().stream()
                .flatMap(block -> block.getFloors().stream())
                .flatMap(floor -> floor.getFlats().stream())
                .count();
    }


    /**
     * Metoda zwraca ilość mieszkań, które mają conajmniej tyle pokoi ile przekazano przez parametr
     */
    @Override
    public long getNumberOfFlats(int minimumRoomNumber) {
        return area.getBlocks().stream()
                .flatMap(block -> block.getFloors().stream())
                .flatMap(floor -> floor.getFlats().stream())
                .filter(flat -> flat.getRoomsCount() >= minimumRoomNumber)
                .count();
    }



    //---------------------------------------------------------------------toMap/toSet

    /**
     * Metoda zwraca mapę w której kluczem jest numer bloku
     * a wartością materiał z jakiego został wykonany
     */
    @Override
    public Map<Integer, Material> blockNumberToMaterial() {
        //ROZWIAZANIE BEZ STREAM
        Map<Integer, Material> map = new HashMap<>();

        for (Block block : area.getBlocks()) {
            map.put(block.getBlockNumber(), block.getMaterial());
        }

        //return map;

        //ROZWIAZANIE Z UZYCIEM STREAM
        return area.getBlocks().stream()
                .collect(Collectors.toMap(
                        block -> block.getBlockNumber(),
                        block -> block.getMaterial()
                ));
    }

    /**
     * Metoda zwraca mapę w której kluczem jest materiał z jakiego został wykonany blok
     * a wartością jest ilość pię†er w danym bloku
     */
    @Override
    public Map<Material, Integer> blockMaterialToFloorCount() {
        return area.getBlocks().stream()
                .collect(Collectors.toMap(
                        block -> block.getMaterial(),
                        block -> block.getFloors().size()
                ));
    }

    /**
     * Metoda zwraca mapę w której kluczem jest numer piętra
     * a wartością jest boolean sprawdzający czy dane piętro ma patio
     */
    @Override
    public Map<Integer, Boolean> floorNumberToPatio() {
        return area.getBlocks().stream()
                .flatMap(block -> block.getFloors().stream())
                .collect(Collectors.toMap(
                        floor -> floor.getFloorNumber(),
                        floor -> floor.isWithPatio(),
                        (first , second) -> first
                ));
    }

    /**
     * Metoda zwraca mapę w której kluczem jest blok
     * a wartością jest lista pięter, które zawiera
     */
    @Override
    public Map<Block, List<Floor>> blockToFloors() {
        return area.getBlocks().stream()
                .collect(Collectors.toMap(
                        Function.identity(), // block -> block
                        block -> block.getFloors()
                ));
    }

    /**
     * Metoda zwraca zbiór 10 mieszkań
     */
    @Override
    public Set<Flat> tenFlats() {
        return area.getBlocks().stream()
                .flatMap(block -> block.getFloors().stream())
                .flatMap(floor -> floor.getFlats().stream())
                .limit(10)
                .collect(Collectors.toSet());

    }


    //---------------------------------------------------------------------joining

    /**
     * Metoda zwraca listę właścicieli mieszkań połączone znakiem ", "
     * Usuń duplikaty
     *
     * Wejscie ["Kowalski" "Nowak" "Kwiatkowski"]
     * Wyjście "Kowalski, Nowak, Kwiatkowski"
     */
    @Override
    public String owners() {
        return area.getBlocks().stream()
                .flatMap(block -> block.getFloors().stream())
                .flatMap(block -> block.getFlats().stream())
                .map(flat -> flat.getOwner())
                .distinct()
                .collect(Collectors.joining(", "));

    }

    /**
     * Metoda zwraca listę właścicieli mieszkań połączone znakiem ", "
     * Napisz znak "!" na początku i na końcu już połączonych nazwisk
     * Usuń duplikaty
     *
     * Wejscie ["Kowalski" "Nowak" "Kwiatkowski"]
     * Wyjście "!Kowalski, Nowak, Kwiatkowski!"
     */
    @Override
    public String owners2() {
        return area.getBlocks().stream()
                .flatMap(block -> block.getFloors().stream())
                .flatMap(block -> block.getFlats().stream())
                .map(flat -> flat.getOwner())
                .distinct()
                .collect(Collectors.joining(", ", "!", "!"));

    }


    /**
     * Metoda zwraca listę właścicieli mieszkań połączone znakiem ", \n"
     * W pierwszej linii napisz. "Mieszkają z nami: \n"
     * W ostatniej napisz. "\nDołącz do nich. Kup mieszkanie już dziś!"
     * Usuń duplikaty
     *
     * Wejscie ["Kowalski" "Nowak" "Kwiatkowski"]
     * Wyjście  "Mieszkają z nami:
     *          Kowalski,
     *          Nowak,
     *          Kwiatkowski
     *          Dołącz do nich. Kup mieszkanie już dziś!"
     */
    @Override
    public String ownersPromo() {
        return area.getBlocks().stream()
                .flatMap(block -> block.getFloors().stream())
                .flatMap(block -> block.getFlats().stream())
                .map(flat -> flat.getOwner())
                .distinct()
                .collect(Collectors.joining(
                        ", \n",
                        "Mieszkają z nami: \n",
                        "\nDołącz do nich. Kup mieszkanie już dziś!"));

    }

    /**
     * Metoda zwraca listę właścicieli mieszkań połączone znakiem przekazanym przez parametr
     Prefix oraz suffix również zostaną przekazane przez parametr
     * Usuń duplikaty
     *
     */
    @Override
    public String ownersWithPrefixAndSuffix(String delimiter, String prefix, String suffix) {
        return area.getBlocks().stream()
                .flatMap(block -> block.getFloors().stream())
                .flatMap(block -> block.getFlats().stream())
                .map(flat -> flat.getOwner())
                .distinct()
                .collect(Collectors.joining(
                        delimiter,
                        prefix,
                        suffix));

    }



    //---------------------------------------------------------------------groupingBy

    /**
     * Metoda grupuje budynki po materiale z którego zostały wykonane
     *
     */
    @Override
    public Map<Material, List<Block>> groupByMaterial() {
        return area.getBlocks().stream()
                .collect(Collectors.groupingBy(block -> block.getMaterial()));
    }

    /**
     * Metoda grupuje budynki po ilości pięter
     *
     */
    @Override
    public Map<Integer, List<Block>> groupByFloorNumber() {
        return area.getBlocks().stream()
                .collect(Collectors.groupingBy(block -> block.getFloors().size()));
    }

    /**
     * Metoda grupuje piętra pod względem posiadania patio
     *
     */
    @Override
    public Map<Boolean, List<Floor>> groupByPatio() {
        return area.getBlocks().stream()
                .flatMap(block -> block.getFloors().stream())
                .collect(Collectors.groupingBy(floor -> floor.isWithPatio()));
    }

    /**
     * Metoda grupuje mieszkania pod względem posiadania basenu
     *
     */
    @Override
    public Map<Boolean, List<Flat>> groupByPool() {
        return area.getBlocks().stream()
                .flatMap(block -> block.getFloors().stream())
                .flatMap(floor -> floor.getFlats().stream())
                .collect(Collectors.groupingBy(flat -> flat.isWithPool()));
    }

    /**
     * [UWAGA TRUDNE]
     * Metoda grupuje piętra pod względem posiadania basenu
     * HINT: trzeba zrobić nowy stream wewnątrz collecta
     */
    @Override
    public Map<Boolean, List<Floor>> groupFloorsByPool() {
        return area.getBlocks().stream()
                .flatMap(block -> block.getFloors().stream())
                .collect(Collectors
                        .groupingBy(floor -> floor.getFlats()
                                .stream()
                                .anyMatch(flat -> flat.isWithPool())
                        ));
    }


    //--------------------------------------------------------------------- counting()

    /**
     * Metoda zlicza ile budynków zostało wykonanych z jakiego materiału
     *
     */
    @Override
    public Map<Material, Long> countByMaterial() {
        return area.getBlocks().stream()
                .collect(Collectors.groupingBy(
                        block -> block.getMaterial(),
                        Collectors.counting())
                );
    }

    /**
     * Metoda zlicza ile budynków posiada ile pięter
     *
     */
    @Override
    public Map<Integer, Long> countByFloorNumber() {
        return area.getBlocks().stream()
                .collect(Collectors.groupingBy(
                        block -> block.getFloors().size(),
                        Collectors.counting())
                );
    }

    /**
     * Metoda zlicza ile pięter posiada/nie posiada patio
     *
     */
    @Override
    public Map<Boolean, Long> countByPatio() {
        return area.getBlocks().stream()
                .flatMap(block -> block.getFloors().stream())
                .collect(Collectors.groupingBy(
                        floor -> floor.isWithPatio(),
                        Collectors.counting())
                );
    }

    /**
     * Metoda zlicza piętra pod względem ilości mieszkań
     *
     */
    @Override
    public Map<Integer, Long> countByFlatNumber() {
        return area.getBlocks().stream()
                .flatMap(block -> block.getFloors().stream())
                .collect(Collectors.groupingBy(
                        floor -> floor.getFlats().size(),
                        Collectors.counting())
                );
    }


    /**
     * Metoda zlicza ile mieszkań posiada/nie posiada basenu
     *
     */
    @Override
    public Map<Boolean, Long> countByPool() {
        return area.getBlocks().stream()
                .flatMap(block -> block.getFloors().stream())
                .flatMap(floor -> floor.getFlats().stream())
                .collect(Collectors.groupingBy(
                        flat -> flat.isWithPool(),
                        Collectors.counting())
                );
    }

    /**
     * Metoda zlicza ilość mieszkań względem ilości pokoi
     *
     */
    @Override
    public Map<Integer, Long> countByRoomsNumber() {
        return area.getBlocks().stream()
                .flatMap(block -> block.getFloors().stream())
                .flatMap(floor -> floor.getFlats().stream())
                .collect(Collectors.groupingBy(
                        flat -> flat.getRoomsCount(),
                        Collectors.counting())
                );
    }


    /**
     * [UWAGA TRUDNE]
     * Metoda zlicza ile pięter posiada basen
     *
     * HINT: trzeba zrobić nowy stream wewnątrz collecta
     */
    @Override
    public Map<Boolean, Long> groupFlatsByPool() {
        return area.getBlocks().stream()
                .flatMap(block -> block.getFloors().stream())
                .collect(Collectors
                        .groupingBy(floor -> floor.getFlats()
                                        .stream()
                                        .anyMatch(flat -> flat.isWithPool()
                                        ),
                                Collectors.counting()
                        ));
    }


    //--------------------------------------------------------------------- summingInt()

    /**
     * Metoda sumuje numery bloków względem materiału z którego są wykonane
     *
     */
    @Override
    public Map<Material, Integer> sumBlockNumbers() {
        return area.getBlocks().stream()
                .collect(Collectors.groupingBy(
                        block -> block.getMaterial(),
                        Collectors.summingInt(block -> block.getBlockNumber()))
                );
    }

    /**
     * Metoda sumuje ilość mieszkań, względem numeru piętra
     *
     */
    @Override
    public Map<Integer, Integer> sumFlatCountOnFloor() {
        return area.getBlocks().stream()
                .flatMap(block -> block.getFloors().stream())
                .collect(Collectors.groupingBy(
                        floor -> floor.getFloorNumber(),
                        Collectors.summingInt(floor -> floor.getFlats().size()))
                );
    }

    /**
     * Metoda sumuje ilość mieszkań, względem tego czy na danym piętrze jest patio
     *
     */
    @Override
    public Map<Boolean, Integer> sumFlatCountWithPatio() {
        return area.getBlocks().stream()
                .flatMap(block -> block.getFloors().stream())
                .collect(Collectors.groupingBy(
                        floor -> floor.isWithPatio(),
                        Collectors.summingInt(floor -> floor.getFlats().size()))
                );
    }

    /**
     * Metoda sumuje ceny mieszkań, względem nazwiska właściciela
     *
     */
    @Override
    public Map<String, Long> sumPriceWithPool() {
        return area.getBlocks().stream()
                .flatMap(block -> block.getFloors().stream())
                .flatMap(floor -> floor.getFlats().stream())
                .collect(Collectors.groupingBy(
                        flat -> flat.getOwner(),
                        Collectors.summingLong(flat -> flat.getPrice()))
                );
    }

}
