package pl.sda.programming1.stream.agency.exercise.model;

import java.util.List;

public class Floor {

    private final List<Flat> flats;
    private final int floorNumber;
    private final boolean withPatio;

    public Floor(int floorNumber, boolean withPatio, List<Flat> flats) {
        this.floorNumber = floorNumber;
        this.withPatio = withPatio;
        this.flats = flats;
    }

    public List<Flat> getFlats() {
        return flats;
    }

    public int getFloorNumber() {
        return floorNumber;
    }

    public boolean isWithPatio() {
        return withPatio;
    }

    @Override
    public String toString() {
        return "Floor{" +
                "flats=" + flats +
                ", floorNumber=" + floorNumber +
                ", withPatio=" + withPatio +
                '}';
    }
}
