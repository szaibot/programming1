package pl.sda.programming1.stream.agency.exercise.model;

import java.util.List;

public class Area {
    private final List<Block> blocks;
    private final String ownerName;
    private final String name;

    public Area(String ownerName, String name, List<Block> blocks) {
        this.ownerName = ownerName;
        this.name = name;
        this.blocks = blocks;
    }

    public List<Block> getBlocks() {
        return blocks;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Area{" +
                "blocks=" + blocks +
                ", ownerName='" + ownerName + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
