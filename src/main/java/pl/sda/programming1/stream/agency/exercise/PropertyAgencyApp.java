package pl.sda.programming1.stream.agency.exercise;

class PropertyAgencyApp {
    public static void main(String[] args) {
        //UNCOMMENT FOR SOLUTION REVIEW
        //AgencyRepository repository = new AgencyRepositoryAnswer();
        AgencyRepository repository = new AgencyRepositoryExercise();

        System.out.println(repository.getAreaName());
    }
}
