package pl.sda.programming1.stream.agency.exercise.model;

import java.util.List;

public class Block {
    private final List<Floor> floors;
    private final Integer blockNumber;
    private final Material material;

    public Block(Integer blockNumber, Material material, List<Floor> floors) {
        this.blockNumber = blockNumber;
        this.material = material;
        this.floors = floors;
    }

    public List<Floor> getFloors() {
        return floors;
    }

    public Integer getBlockNumber() {
        return blockNumber;
    }

    public Material getMaterial() {
        return material;
    }

    @Override
    public String toString() {
        return "Block{" +
                "floors=" + floors +
                ", blockNumber=" + blockNumber +
                ", material=" + material +
                '}';
    }
}
