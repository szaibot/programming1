package pl.sda.programming1.stream.agency.exercise;

import pl.sda.programming1.stream.agency.exercise.model.*;

import java.util.*;

import static pl.sda.programming1.stream.agency.exercise.model.Material.*;

public class AgencyRepositoryExercise implements AgencyRepository {

    public static final Area area = createArea();

    /**
     * Zwróć nazwę osiedla (metoda rozgrzewkowa - bez uzycia streama)
     */
    public String getAreaName() {
        throw new IllegalStateException("NOT IMPLEMENTED!");
    }

    /**
     * Zwróć właściciela osiedla (metoda rozgrzewkowa - bez uzycia streama)
     */
    public String getAreaOwner() {
        throw new IllegalStateException("NOT IMPLEMENTED!");
    }

    /**
     * Zwróć wszystkie bloki (metoda rozgrzewkowa - bez uzycia streama)
     */
    public List<Block> getBlocks() {
        throw new IllegalStateException("NOT IMPLEMENTED!");
    }


    /**
     * Metoda zwraca ilość budynków na osiedlu
     */
    @Override
    public long buildingsCount() {
        throw new IllegalStateException("NOT IMPLEMENTED!");
    }

    //THEORY PART 1

    /**
     * Metoda zwraca ilość budynków ze szkła na osiedlu
     * Przerób rozwiązanie na streama
     */
    @Override
    public long getGlassBlocks() {
        List<Block> blocks = new ArrayList<>();

        for (Block block : area.getBlocks()) {
            if (block.getMaterial() == GLASS) {
                blocks.add(block);
            }
        }

        return blocks.size();
    }

    /**
     * Metoda zwraca wszystkie budynki z betonu
     * Zaimplementuj najpierw z użyciem pętli for a potem streama
     */
    @Override
    public  List<Block> getConcreteBlocks() {
        throw new IllegalStateException("NOT IMPLEMENTED!");
    }

    /**
     * Metoda zwraca wszystkie budynki z materiału przekazanego jako parametr
     */
    @Override
    public List<Block> getBlocks(Material material) {
        throw new IllegalStateException("NOT IMPLEMENTED!");
    }


    //THEORY PART 2
    //---------------------------------------------------------------------flatMap


    /**
     * Metoda zwraca wszystkie piętra we wszystkich budynkach
     * Zaimplementuj najpierw z użyciem pętli for a potem streama
     */
    @Override
    public List<Floor> getFloors() {
        throw new IllegalStateException("NOT IMPLEMENTED!");
    }

    /**
     * Metoda zwraca ilość wszystkich pięter
     */
    @Override
    public long getFloorsCount() {
        throw new IllegalStateException("NOT IMPLEMENTED!");
    }

    /**
     * Metoda zwraca piętra, które mają patio
     */
    @Override
    public List<Floor> getFloorsWithPatio() {
        throw new IllegalStateException("NOT IMPLEMENTED!");
    }

    /**
     * Metoda zwraca piętra, które są powyżej 2 poziomu
     * (piętra numerowane są od 1)
     */
    @Override
    public List<Floor> getAboveSecondFloor() {
        throw new IllegalStateException("NOT IMPLEMENTED!");
    }


    /**
     * Metoda zwraca piętra, które są z poziomu przekazanego jako parametr.
     */
    @Override
    public List<Floor> getFloors(int floorNumber) {
        throw new IllegalStateException("NOT IMPLEMENTED!");
    }


    /**
     * Metoda zwraca wszystkie mieszkania na wszystkich piętrach we wszystkich blokach
     */
    @Override
    public List<Flat> getFlats() {
        throw new IllegalStateException("NOT IMPLEMENTED!");
    }

    /**
     * Metoda zwraca wszystkie mieszkania z basenem
     */
    @Override
    public List<Flat> getFlatsWithPool() {
        throw new IllegalStateException("NOT IMPLEMENTED!");
    }

    /**
     * Metoda zwraca nazwiska właścicieli wszystkich mieszkań na osiedlu
     */
    @Override
    public List<String> getFlatsOwners() {
        throw new IllegalStateException("NOT IMPLEMENTED!");
    }

    /**
     * Metoda zwraca ilość mieszkań na całym osiedlu
     */
    @Override
    public long getNumberOfFlats() {
        throw new IllegalStateException("NOT IMPLEMENTED!");
    }


    /**
     * Metoda zwraca ilość mieszkań, które mają conajmniej tyle pokoi ile przekazano przez parametr
     */
    @Override
    public long getNumberOfFlats(int minimumRoomNumber) {
        throw new IllegalStateException("NOT IMPLEMENTED!");
    }



    //---------------------------------------------------------------------toMap/toSet

    /**
     * Metoda zwraca mapę w której kluczem jest numer bloku
     * a wartością materiał z jakiego został wykonany
     */
    @Override
    public Map<Integer, Material> blockNumberToMaterial() {
        throw new IllegalStateException("NOT IMPLEMENTED!");
    }

    /**
     * Metoda zwraca mapę w której kluczem jest materiał z jakiego został wykonany blok
     * a wartością jest ilość pię†er w danym bloku
     */
    @Override
    public Map<Material, Integer> blockMaterialToFloorCount() {
        throw new IllegalStateException("NOT IMPLEMENTED!");
    }

    /**
     * Metoda zwraca mapę w której kluczem jest numer piętra
     * a wartością jest boolean sprawdzający czy dane piętro ma patio
     */
    @Override
    public Map<Integer, Boolean> floorNumberToPatio() {
        throw new IllegalStateException("NOT IMPLEMENTED!");
    }

    /**
     * Metoda zwraca mapę w której kluczem jest blok
     * a wartością jest lista pięter, które zawiera
     */
    @Override
    public Map<Block, List<Floor>> blockToFloors() {
        throw new IllegalStateException("NOT IMPLEMENTED!");
    }

    /**
     * Metoda zwraca zbiór 10 mieszkań
     */
    @Override
    public Set<Flat> tenFlats() {
        throw new IllegalStateException("NOT IMPLEMENTED!");
    }


    //---------------------------------------------------------------------joining

    /**
     * Metoda zwraca listę właścicieli mieszkań połączone znakiem ", "
     * Usuń duplikaty
     *
     * Wejscie ["Kowalski" "Nowak" "Kwiatkowski"]
     * Wyjście "Kowalski, Nowak, Kwiatkowski"
     */
    @Override
    public String owners() {
        throw new IllegalStateException("NOT IMPLEMENTED!");

    }

    /**
     * Metoda zwraca listę właścicieli mieszkań połączone znakiem ", "
     * Napisz znak "!" na początku i na końcu już połączonych nazwisk
     * Usuń duplikaty
     *
     * Wejscie ["Kowalski" "Nowak" "Kwiatkowski"]
     * Wyjście "!Kowalski, Nowak, Kwiatkowski!"
     */
    @Override
    public String owners2() {
        throw new IllegalStateException("NOT IMPLEMENTED!");

    }


    /**
     * Metoda zwraca listę właścicieli mieszkań połączone znakiem ", \n"
     * W pierwszej linii napisz. "Mieszkają z nami: \n"
     * W ostatniej napisz. "\nDołącz do nich. Kup mieszkanie już dziś!"
     * Usuń duplikaty
     *
     * Wejscie ["Kowalski" "Nowak" "Kwiatkowski"]
     * Wyjście  "Mieszkają z nami:
     *          Kowalski,
     *          Nowak,
     *          Kwiatkowski
     *          Dołącz do nich. Kup mieszkanie już dziś!"
     */
    @Override
    public String ownersPromo() {
        throw new IllegalStateException("NOT IMPLEMENTED!");

    }

    /**
     * Metoda zwraca listę właścicieli mieszkań połączone znakiem przekazanym przez parametr
       Prefix oraz suffix również zostaną przekazane przez parametr
     * Usuń duplikaty
     *
     */
    @Override
    public String ownersWithPrefixAndSuffix(String delimiter, String prefix, String suffix) {
        throw new IllegalStateException("NOT IMPLEMENTED!");

    }



    //---------------------------------------------------------------------groupingBy

    /**
     * Metoda grupuje budynki po materiale z którego zostały wykonane
     *
     */
    @Override
    public Map<Material, List<Block>> groupByMaterial() {
        throw new IllegalStateException("NOT IMPLEMENTED!");
    }

    /**
     * Metoda grupuje budynki po ilości pięter
     *
     */
    @Override
    public Map<Integer, List<Block>> groupByFloorNumber() {
        throw new IllegalStateException("NOT IMPLEMENTED!");
    }

    /**
     * Metoda grupuje piętra pod względem posiadania patio
     *
     */
    @Override
    public Map<Boolean, List<Floor>> groupByPatio() {
        throw new IllegalStateException("NOT IMPLEMENTED!");
    }

    /**
     * Metoda grupuje mieszkania pod względem posiadania basenu
     *
     */
    @Override
    public Map<Boolean, List<Flat>> groupByPool() {
        throw new IllegalStateException("NOT IMPLEMENTED!");
    }

    /**
     * [UWAGA TRUDNE]
     * Metoda grupuje piętra pod względem posiadania basenu
     * HINT: trzeba zrobić nowy stream wewnątrz collecta
     */
    @Override
    public Map<Boolean, List<Floor>> groupFloorsByPool() {
        throw new IllegalStateException("NOT IMPLEMENTED!");
    }


    //--------------------------------------------------------------------- counting()

    /**
     * Metoda zlicza ile budynków zostało wykonanych z jakiego materiału
     *
     */
    @Override
    public Map<Material, Long> countByMaterial() {
        throw new IllegalStateException("NOT IMPLEMENTED!");
    }

    /**
     * Metoda zlicza ile budynków posiada ile pięter
     *
     */
    @Override
    public Map<Integer, Long> countByFloorNumber() {
        throw new IllegalStateException("NOT IMPLEMENTED!");
    }

    /**
     * Metoda zlicza ile pięter posiada/nie posiada patio
     *
     */
    @Override
    public Map<Boolean, Long> countByPatio() {
        throw new IllegalStateException("NOT IMPLEMENTED!");
    }

    /**
     * Metoda zlicza piętra pod względem ilości mieszkań
     *
     */
    @Override
    public Map<Integer, Long> countByFlatNumber() {
        throw new IllegalStateException("NOT IMPLEMENTED!");
    }


    /**
     * Metoda zlicza ile mieszkań posiada/nie posiada basenu
     *
     */
    @Override
    public Map<Boolean, Long> countByPool() {
        throw new IllegalStateException("NOT IMPLEMENTED!");
    }

    /**
     * Metoda zlicza ilość mieszkań względem ilości pokoi
     *
     */
    @Override
    public Map<Integer, Long> countByRoomsNumber() {
        throw new IllegalStateException("NOT IMPLEMENTED!");
    }


    /**
     * [UWAGA TRUDNE]
     * Metoda zlicza ile pięter posiada basen
     *
     * HINT: trzeba zrobić nowy stream wewnątrz collecta
     */
    @Override
    public Map<Boolean, Long> groupFlatsByPool() {
        throw new IllegalStateException("NOT IMPLEMENTED!");
    }


    //--------------------------------------------------------------------- summingInt()

    /**
     * Metoda sumuje numery bloków względem materiału z którego są wykonane
     *
     */
    @Override
    public Map<Material, Integer> sumBlockNumbers() {
        throw new IllegalStateException("NOT IMPLEMENTED!");
    }

    /**
     * Metoda sumuje ilość mieszkań, względem numeru piętra
     *
     */
    @Override
    public Map<Integer, Integer> sumFlatCountOnFloor() {
        throw new IllegalStateException("NOT IMPLEMENTED!");
    }

    /**
     * Metoda sumuje ilość mieszkań, względem tego czy na danym piętrze jest patio
     *
     */
    @Override
    public Map<Boolean, Integer> sumFlatCountWithPatio() {
        throw new IllegalStateException("NOT IMPLEMENTED!");
    }

    /**
     * Metoda sumuje ceny mieszkań, względem nazwiska właściciela
     * HINT inny summing niż summingInt() ;)
     */
    @Override
    public Map<String, Long> sumPriceWithPool() {
        throw new IllegalStateException("NOT IMPLEMENTED!");
    }

    private static Area createArea() {
        return new Area("Kowalski", "Illumino", Arrays.asList(
                new Block(1, CONCRETE, Arrays.asList(
                        new Floor(1, false, Arrays.asList(
                                new Flat(1, 4, "Nowak", false, 200_000),
                                new Flat(2, 4, "Wiśniewski", false, 200_000),
                                new Flat(3, 5, "Wójcik", false, 300_000),
                                new Flat(4, 5, "Kowalczyk", false, 300_000)
                        )),
                        new Floor(2, false, Arrays.asList(
                                new Flat(5, 4, "Kaminski", false, 200_000),
                                new Flat(6, 4, "Lewandowski", false, 200_000),
                                new Flat(7, 5, "Zielinski", false, 300_000),
                                new Flat(8, 5, "Szymanski", false, 300_000)
                        )),
                        new Floor(3, true, Arrays.asList(
                                new Flat(9, 4, "Woźniak", false, 210_000),
                                new Flat(10, 4, "Kozłowski", false, 210_000),
                                new Flat(11, 5, "Jankowski", true, 400_000),
                                new Flat(12, 5, "Mazur", true, 400_000)
                        )),
                        new Floor(4, true, Arrays.asList(
                                new Flat(13, 4, "Kaminski", false, 200_000),
                                new Flat(14, 4, "Kowalczyk", false, 200_000),
                                new Flat(15, 5, "Szymanski", true, 400_000),
                                new Flat(16, 5, "Nowak", true, 400_000)
                        ))
                )),
                new Block(2, GLASS, Arrays.asList(
                        new Floor(10, false, Arrays.asList(
                                new Flat(100, 4, "Kaminski", false, 250_000),
                                new Flat(200, 4, "Lewandowski", false, 250_000),
                                new Flat(300, 5, "Zielinski", false, 400_000),
                                new Flat(400, 5, "Szymanski", false, 400_000),
                                new Flat(500, 2, "Szymanski", false, 150_000),
                                new Flat(600, 3, "Zielinski", false, 300_000),
                                new Flat(700, 6, "Nowak", true, 500_000)
                        )),
                        new Floor(20, true, Arrays.asList(
                                new Flat(800, 4, "Kowalczyk", false, 250_000),
                                new Flat(900, 4, "Nowak", false, 250_000),
                                new Flat(1000, 5, "Zielinski", false, 250_000),
                                new Flat(1100, 5, "Nowak", false, 250_000),
                                new Flat(1200, 2, "Szymanski", false, 150_000),
                                new Flat(1300, 3, "Zielinski", false, 300_000),
                                new Flat(1400, 6, "Jankowski", true, 500_000)
                        )),
                        new Floor(30, true, Arrays.asList(
                                new Flat(1500, 4, "Wojciechowski", false, 250_000),
                                new Flat(1600, 4, "Piotrowski", false, 250_000),
                                new Flat(1700, 5, "Grabowski", false, 250_000),
                                new Flat(1800, 5, "Wojciechowski", false, 250_000),
                                new Flat(1900, 2, "Kwiatkowski", false, 150_000),
                                new Flat(2000, 3, "Kaczmarek", false, 300_000),
                                new Flat(2100, 6, "Krawczyk", true, 500_000)
                        ))
                )),
                new Block(3, STEEL, Arrays.asList(
                        new Floor(100, false, Arrays.asList(
                                new Flat(10000, 7, "Nowak", true, 1_000_000),
                                new Flat(20000, 7, "Kowalski", true, 1_000_000)
                        )),
                        new Floor(200, false, Arrays.asList(
                                new Flat(30000, 7, "Kowalski", true, 1_200_000),
                                new Flat(40000, 7, "Wiśniewski", true, 1_200_000)
                        )),
                        new Floor(300, true, Arrays.asList(
                                new Flat(50000, 7, "Kamiński", true, 1_200_000),
                                new Flat(60000, 7, "Lewandowski", true, 1_200_000)
                        )),
                        new Floor(400, true, Arrays.asList(
                                new Flat(70000, 7, "Kowalski", true, 1_200_000),
                                new Flat(80000, 7, "Wiśniewski", true, 1_200_000)
                        )),
                        new Floor(500, false, Arrays.asList(
                                new Flat(90000, 7, "Kowalski", true, 1_200_000),
                                new Flat(100000, 7, "Szymański", true, 1_200_000)
                        )),
                        new Floor(600, false, Arrays.asList(
                                new Flat(110000, 7, "Zieliński", true, 1_200_000),
                                new Flat(120000, 7, "Woźniak", true, 1_200_000)
                        )),
                        new Floor(700, true, Arrays.asList(
                                new Flat(130000, 7, "Wójcik", true, 1_200_000),
                                new Flat(140000, 7, "Wiśniewski", true, 1_200_000)
                        )),
                        new Floor(800, true, Arrays.asList(
                                new Flat(150000, 7, "Nowak", true, 1_300_000),
                                new Flat(160000, 7, "Dąbrowski", true, 1_300_000)
                        ))
                ))
        ));
    }
}
