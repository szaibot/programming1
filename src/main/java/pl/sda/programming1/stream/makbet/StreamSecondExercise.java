package pl.sda.programming1.stream.makbet;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.regex.Pattern;
import java.util.stream.Stream;

//Exercise related to homework from first classes
public class StreamSecondExercise {

    public static void main(String[] args) throws IOException {
        Stream<String> lines = Files.lines(Paths.get("/Users/tobiakow/makbet.txt")); //TODO CHANGE PATH TO FILE
        StreamSecondExercise streamSecondExercise = new StreamSecondExercise();

        String partialText = "AKT PIERWSZY\n" +
                "\n" +
                "\n" +
                "\n" +
                "SCENA I\n" +
                "\n" +
                "/ Otwarte pole. /\n" +
                "\n" +
                "/ Grzmoty i błyskawice. — Wchodzą trzy Czarownice. /\n" +
                "\n" +
                "\n" +
                "1 CZAROWNICA\n" +
                "\n" +
                "Gdzie, kiedy nowe spotkanie?\n" +
                "W grzmotach, błyskach, Król, huraganie?\n";


        print(lines);
        print(toStream(partialText));

        //FIRST TASK FROM PDF file
        streamSecondExercise.countLines(lines);
    }

    private static Stream<String> toStream(String text) {
        return Pattern.compile("\\n").splitAsStream(text);
    }

    private static void print(Stream<String> strings) {
        strings.forEach(System.out::println);
    }

    public long countLines(Stream<String> stream) {
        throw new UnsupportedOperationException();
    }

    public void printNotEmptyLines(Stream<String> stream) {
        throw new UnsupportedOperationException();
    }

    public long countNotEmptyLines(Stream<String> stream) {
        throw new UnsupportedOperationException();
    }

    public long countWords(Stream<String> stream) {
        throw new UnsupportedOperationException();
    }

    public long countUniqueWords(Stream<String> stream) {
        throw new UnsupportedOperationException();
    }

    public void printUniqueWords(Stream<String> stream) {
        throw new UnsupportedOperationException();
    }

    public String makeAllCaps(Stream<String> stream) {
        throw new UnsupportedOperationException();
    }

    public String removeCapitilized(Stream<String> stream) {
        throw new UnsupportedOperationException();
    }

    public String wordToCount(Stream<String> stream) {
        throw new UnsupportedOperationException();
    }

    public String kingToCesar(Stream<String> stream) {
        throw new UnsupportedOperationException();
    }

}
