package pl.sda.programming1.stream.makbet;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class StreamSecondAnswer extends StreamSecondExercise {

    public StreamSecondAnswer() {
    }

    public static void main(String[] args) throws IOException {
        Stream<String> lines = Files.lines(Paths.get("/Users/tobiakow/makbet.txt"));
        StreamSecondAnswer streamSecondAnswer = new StreamSecondAnswer();

        String text = "AKT PIERWSZY\n" +
                "\n" +
                "\n" +
                "\n" +
                "SCENA I\n" +
                "\n" +
                "/ Otwarte pole. /\n" +
                "\n" +
                "/ Grzmoty i błyskawice. — Wchodzą trzy Czarownice. /\n" +
                "\n" +
                "\n" +
                "1 CZAROWNICA\n" +
                "\n" +
                "Gdzie, kiedy nowe spotkanie?\n" +
                "W grzmotach, błyskach, Król, huraganie?\n";


        System.out.println(streamSecondAnswer.countLines(toStream(text)));
        streamSecondAnswer.printNotEmptyLines(toStream(text));
        System.out.println(streamSecondAnswer.countNotEmptyLines(toStream(text)));
        System.out.println("all words: " + streamSecondAnswer.countWords(toStream(text)));
        System.out.println("unique words: " + streamSecondAnswer.countUniqueWords(toStream(text)));
        streamSecondAnswer.printUniqueWords(toStream(text));
        System.out.println("all caps:" + streamSecondAnswer.makeAllCaps(toStream(text)));
        System.out.println("without capitalized:" + streamSecondAnswer.removeCapitilized(toStream(text)));
        System.out.println("word to count" + streamSecondAnswer.wordToCount(toStream(text)));
        System.out.println("king to cesarz" + streamSecondAnswer.kingToCesar(toStream(text)));
    }

    private static Stream<String> toStream(String text) {
        return Pattern.compile("\\n").splitAsStream(text);
    }

    @Override
    public long countLines(Stream<String> stream) {
        return stream.count();
    }

    @Override
    public void printNotEmptyLines(Stream<String> stream) {
        stream
            .filter(s -> !s.isEmpty())
            .forEach(System.out::println);
    }

    @Override
    public long countNotEmptyLines(Stream<String> stream) {
        return stream
                .filter(s -> !s.isEmpty())
                .count();
    }

    @Override
    public long countWords(Stream<String> stream) {
         return stream.filter(s -> !s.isEmpty())
                 .flatMap(line -> Stream.of(line.split("\\s")))
                 .count();
    }

    @Override
    public long countUniqueWords(Stream<String> stream) {
        Map<String, Integer> collect = stream.filter(s -> !s.isEmpty())
                .flatMap(line -> Stream.of(line.split("\\s")))
                .map(String::toLowerCase)
                .collect(Collectors.toMap(word -> word, word -> 1, Integer::sum));

        return collect.entrySet().stream()
                .filter(entry -> entry.getValue() == 1)
                .count();
    }

    @Override
    public void printUniqueWords(Stream<String> stream) {
        stream.filter(s -> !s.isEmpty())
                .flatMap(line -> Stream.of(line.split("\\s")))
                .map(String::toLowerCase)
                .collect(Collectors.toMap(word -> word, word -> 1, Integer::sum))
                .entrySet().stream()
                .filter(entry -> entry.getValue() == 1)
                .forEach((entry) -> System.out.println(entry.getKey()));
    }

    @Override
    public String makeAllCaps(Stream<String> stream) {
        return stream.map(line -> Pattern.compile(" ").splitAsStream(line)
                    .map(s -> s.contains("s") ? s.toUpperCase() : s)
                    .collect(Collectors.joining(" ")))
                .collect(Collectors.joining("\n"));
    }

    @Override
    public String removeCapitilized(Stream<String> stream) {
        return stream.map(line -> Pattern.compile(" ").splitAsStream(line)
                    .filter(s -> s.isEmpty() || !Character.isUpperCase(s.charAt(0)))
                    .collect(Collectors.joining(" ")))
                .collect(Collectors.joining("\n"));
    }

    @Override
    public String wordToCount(Stream<String> stream) {
        return stream.map(line -> Pattern.compile(" ").splitAsStream(line)
                .map(s -> String.valueOf(s.length()))
                .collect(Collectors.joining(" ")))
                .collect(Collectors.joining("\n"));
    }

    @Override
    public String kingToCesar(Stream<String> stream) {
        return stream.map(line -> Pattern.compile(" ").splitAsStream(line)
                    .map(s -> "Król".equals(s) ? "Cesarz" : s)
                    .collect(Collectors.joining(" ")))
                .collect(Collectors.joining("\n"));
    }

}
