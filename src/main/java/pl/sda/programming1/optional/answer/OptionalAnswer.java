package pl.sda.programming1.optional.answer;

import pl.sda.programming1.optional.exercise.OptionalExercise;

import java.util.List;
import java.util.Optional;

class OptionalAnswer extends OptionalExercise {

    @Override
    public Optional<String> method0(String s) {
        return Optional.ofNullable(s);
    }

    @Override
    public Optional<Integer> method1(Integer integer) {
        if (integer == null) {
            return Optional.empty();
        } else {
            return Optional.of(integer);
        }

        //krótsza wersja - ternary operator
        //return integer == null ? Optional.empty() : Optional.of(integer);
    }

    @Override
    public String method2(String s) {
        return Optional.ofNullable(s)
                .orElse("emptyString");
    }

    @Override
    public String method3(String s) {
        return Optional.ofNullable(s)
                .orElseThrow(RuntimeException::new);
    }

    @Override
    public String method4(String s) {
        return Optional.ofNullable(s)
                .map(String::toUpperCase)
                .orElse("");
    }

    @Override
    public Optional<String> method5(List<String> strings, String s) {
        return strings.stream()
                .filter(string -> string.contains(s))
                .findAny();
    }

    @Override
    public String method6(List<String> strings, String s) {
        return strings.stream()
                .filter(string -> string.contains(s))
                .findAny()
                .orElseThrow(RuntimeException::new);
    }

    @Override
    public Optional<String> method7(List<String> strings, String s) {
        return strings.stream()
                .filter(string -> string.startsWith(s))
                .findFirst();
    }

    @Override
    public void method8(String string) {
        Optional.ofNullable(string)
                .ifPresent(s -> System.out.println(s));
    }
}
