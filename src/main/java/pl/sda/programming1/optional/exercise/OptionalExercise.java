package pl.sda.programming1.optional.exercise;

import java.util.List;
import java.util.Optional;

public class OptionalExercise {

    /**
     * Metoda opakowuje stringa w optionala
     */
    public Optional<String> method0(String s) {
        throw new UnsupportedOperationException();
    }

    /**
     * Metoda opakowuje zmienną w optionala.
     * Nie używaj metody ofNullable(), ale jeżeli zostanie przekazany null to odpowiednio zareaguj.
     *
     * ZAKAZANA W TEJ METODZIE
     * Optional.ofNullable() - używamy wtedy gdy nie wiemy
     *                                   // co jest w środku
     *
     * DOZWOLONE W TEJ METODZIE
     * Optional.empty() - używamy jak wiemy, że mamy nulla
     * Optional.of() - używamy jak wiemy, że mamy wartość
     *
     * Ta metoda jest tylko do nauki optionala. W realym świecie po to stworzyli metode ofNullable(),
     * żeby jej użyć i się nie zastanawiać ;)
     */
    public Optional<Integer> method1(Integer integer) {
        throw new UnsupportedOperationException();
    }

    /**
     * Jeżeli parametr jest null'em, zwróć string "emptyString".
     * Nie używaj konstrukcji warunkowych if, else.
     */
    public String method2(String s) {
        throw new UnsupportedOperationException();
    }


    /**
     * Jeżeli parametr jest null'em, rzuć exception: RuntimeException().
     * Nie używaj konstrukcji warunkowych if, else.
     */
    public String method3(String s) {
        throw new UnsupportedOperationException();
    }

    /**
     * Zmień małe litery w wejściowym stringu na wielkie litery.
     * Jeżeli string jest null'em, zwróć pusty String: ""
     *
     * Nie używaj konstrukcji warunkowych if, else.
     */
    public String method4(String s) {
        throw new UnsupportedOperationException();
    }

    /**
     * Metoda sprawdza, czy jest jakiś string na liście, który zawiera w sobie string przekazany jako drugi parametr.
     */
    public Optional<String> method5(List<String> strings, String s) {
        throw new UnsupportedOperationException();
    }

    /**
     * Metoda sprawdza, czy jest jakiś string na liście, który zawiera w sobie string przekazany jako drugi parametr.
     * Jeżeli lista nie posiada takiego elementu rzuca wyjątek: RuntimeException
     *
     */
    public String method6(List<String> strings, String s) {
        throw new UnsupportedOperationException();
    }

    /**
     * Metoda znajduje pierszy string, który zaczyna się od znaków przekazanych jako drugi parametr
     *
     */
    public Optional<String> method7(List<String> strings, String s) {
        throw new UnsupportedOperationException();
    }

    /**
     * Metoda wyświetla string jeżeli nie jest nullem.
     * Nie używaj konstrukcji warunkowych if, else.
     */
    public void method8(String string) {
        throw new UnsupportedOperationException();
    }


}
