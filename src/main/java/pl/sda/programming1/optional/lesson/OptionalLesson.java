package pl.sda.programming1.optional.lesson;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

class OptionalLesson {

    public static void main(String[] args) {
        //*************************************************************** creation
        //powiedzieć do czego służy prywatny konstruktor

        Optional<String> notNullObject = Optional.of("notNullObject");

        Optional<Object> nullObject = Optional.ofNullable(null);
        Optional<String> nullString = Optional.ofNullable(null);

        Optional<String> maybeNull = Optional.ofNullable("notNull");

        //Uncomment and explain why this throw NPE
        //Optional.of(null);

        Optional<Object> emptyOptional = Optional.empty();

        String string12346 = null;
        //POLECI NPE tutaj - wytłumacz dlaczego
        Optional.ofNullable(string12346.toUpperCase())
                .orElse("");

        //*************************************************************** get
        //NIE UZYWAC TEJ METODY - przetestuj na nullu

        Optional<String> text = Optional.ofNullable(null);

        String getString = Optional.ofNullable("string").get();

        //*************************************************************** orElse
        //pokazać dlaczego używasz metody nie nulla.
        String string = Optional.ofNullable(getNull())
                .orElse("differentString");

        System.out.println("OrElse " + string);


        //*************************************************************** orElse vs orElseGet
        System.out.println();
        System.out.println("Before or else");

        Optional.ofNullable("")
                .orElse(returnString());

        System.out.println("After or else");

        System.out.println();
        System.out.println();


        System.out.println("Before or else get");
        String mojString = Optional.<String>ofNullable(null)
                .orElseGet(() -> returnString());

        System.out.println(mojString.length());

        System.out.println("After or else get");

        System.out.println();
        System.out.println();


        //*************************************************************** orElseThrow
        //zamien na null

        Optional.ofNullable("") //"".get()
                .orElseThrow(() -> new IllegalArgumentException());


        //*************************************************************** mapping
        Optional.ofNullable("abc")  //Optional(null)
                //if ( s != null)
                .map(s -> s.length()) // null
                // else {
                .orElseThrow(() -> new RuntimeException());


        Optional<Integer> secondAbc = Optional.ofNullable(getNull())
                .map(s -> s.length());


        //*************************************************************** findAny/findFirst in Stream

        //pusta lista
        List<String> collect = Arrays.asList("a", "b", "c")
                .stream()
                .filter(s -> s.startsWith("d"))
                .collect(Collectors.toList());

        String any = Arrays.asList("a", "b", "c")
                .stream()
                .filter(s -> s.equals("d"))
                .findAny()
                .orElseThrow(() -> new RuntimeException());

        String findAnyString = Arrays.asList("a", "b", "c")
                .stream()
                .filter(s -> s.equals("d"))
                .findAny()
                .orElse("there is no d in list");

        System.out.println(findAnyString);

        String findFirstString = Arrays.asList("a", "b", "cc", "cd")
                .stream()
                .filter(s -> s.startsWith("c"))
                .findFirst()
                .orElseThrow(() -> new RuntimeException());

        System.out.println("Find first " + findFirstString);


        //*************************************************************** isPresent
        Optional<String> optionalString = Optional.of("");

        if (optionalString.isPresent()) {
            String someString = optionalString.get();
        } else {
            throw new IllegalArgumentException();
        }

        //Czym rozwiązanie powyżej jest lepsze od if/else?
        //Jeżeli używasz get() albo isPresent() to zawsze da się to zrobić lepiej!
        // Lepsze rozwiązanie
        String someBetterString = Optional.of("")
                    .orElseThrow(() -> new IllegalArgumentException());


        //*************************************************************** ifPresent

        Optional.of("")
                .ifPresent(connectionUrl -> sendEmail(connectionUrl));


        //*************************************************************** ifPresentOrElse from java 9

//        Optional.of("")
//                .ifPresentOrElse(
//                        emailAddress -> sendEmail(emailAddress),
//                        () -> printWarning()
//                );


        //*************************************************************** real case - implement it
//        findAuthorByTitle(title -> Optional.empty(), "abc")
//            .orElse("Nie mam takiego autora");
    }


    private static void printWarning() {
        System.out.println("No email address provided");
    }

    private static String returnString() {
        System.out.println("inside return string method");
        return "";
    }

    static String getNull() {
        return null;
    }

    private static void sendEmail(String connectionUrl) {
        //connecting to database
    }







    public static Optional<String> findAuthorByTitle(BookRepository repository, String title) {
        return null; //TODO IMPLEMENT
    }

    public interface BookRepository {
        Optional<Book> findByTitle(String title);
    }

    public static class Book {

        private final String title;
        private final String author;

        public Book(String title, String author) {
            this.title = title;
            this.author = author;
        }

        public String getTitle() {
            return title;
        }

        public String getAuthor() {
            return author;
        }
    }
}
