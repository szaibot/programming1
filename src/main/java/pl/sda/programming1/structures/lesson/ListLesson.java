package pl.sda.programming1.structures.lesson;

import java.util.*;

class ListLesson {

    public static void main(String[] args) {
//        1. Utwórz ArrayListę i dodaj do niej nazwy kolorów (typu String) i wypisz ją.
        List<String> colors = new ArrayList<>();
        colors.add("blue");
        colors.add("red");
        colors.add("yellow");
        colors.add("black");
        colors.add("orange");

        System.out.println();
        System.out.println("1." + colors);
        System.out.println();

//        2. Wypisz wartości utworzonej ArrayListy jeden pod drugim.
        System.out.println();

        //albo tak
//        for (String color : colors) {
//            System.out.println("2." + color);
//        }

        //albo forEach
        colors.forEach(s -> System.out.println("2. " + s));
        System.out.println();

//        3. Dodaj do ArrayListy element na konkretnym indeksie.

        colors.add(4, "grey");

        System.out.println();
        System.out.println("3. " + colors);
        System.out.println();

//        4. Pobierz z ArrayListy element pod konkretnym indeksem, w szczególności ostatni.

        System.out.println();
        System.out.println("4. " + colors.get(4));
        System.out.println("4. " + colors.get(4));
        System.out.println("4. " + colors.get(5));
        System.out.println("4. " + colors.get(colors.size() - 1));
        System.out.println();

//        5. Zmodyfikuj element na konkretnym indeksie.

        colors.set(3, "new black");

        System.out.println();
        System.out.println("5. " + colors.get(3));
        System.out.println();

//        6. Usuń element na konkretnym indeksie.

        colors.remove(4);

        System.out.println();
        System.out.println("6. " + colors.get(3));
        System.out.println("6. " + colors.get(4));
        System.out.println();

//        7. Znajdź wybrany element.

        System.out.println();
        System.out.println("7. " + colors.indexOf("red"));
        System.out.println("7. " + colors.indexOf("red1234"));
        System.out.println();

//        8. Posortuj ArrayListę (użyj klasy java.util.Collections).


        System.out.println();
        System.out.println("8. " + colors);
        Collections.sort(colors);
        System.out.println("8. " + colors);
        System.out.println();

//        9. Skopiuj zawartość jednej ArrayListy do drugiej (użyj klasy java.util.Collections).

        //Pierwsze rozwiązanie
//        List<String> newList = new ArrayList<>(colors.size());
//        newList.add("");
//        newList.add("");
//        newList.add("");
//        newList.add("");
//        newList.add("");
//
//        Collections.copy(newList, colors);
//
//        System.out.println();
//        System.out.println("9. " + colors);
//        System.out.println("9. " + newList);
//        System.out.println();

        // Drugie rozwiązanie
        List<String> newList = new ArrayList<>(colors);

        System.out.println();
        System.out.println("9. " + colors);
        System.out.println("9. " + newList);
        System.out.println();

//        10. Potasuj ArrayListę (użyj klasy java.util.Collections).


        System.out.println();
        System.out.println("10. " + newList);
        Collections.shuffle(newList);
        System.out.println("10. " + newList);
        System.out.println();

//        11. Odwróć elementy w ArrayLiście (użyj klasy java.util.Collections).

        System.out.println();
        System.out.println("11. " + newList);
        Collections.reverse(newList);
        System.out.println("11. " + newList);
        System.out.println();

//        12. Utwórz podlistę z istniejącej ArrayListy (np. zawierającą 3 pierwsze elementy).

        System.out.println();
        System.out.println("12. " + newList);

        List<String> subList = newList.subList(0, 3); //inclusive vs exclusive

        System.out.println("12. " + subList);
        System.out.println();

//        13. Napisz metodę porównującą dwie ArrayListy przekazane w parametrze.

        System.out.println("13. " + subList);

        List<String> shuffled = new ArrayList<>(subList);
        Collections.shuffle(shuffled);

        //equals kiedy listy nie są takie same
        System.out.println("13. " + subList.equals(shuffled));

        // pierwsze rozwiązanie
//        if (subList.size() != shuffled.size()) {
//            System.out.println(false);
//        }
//
//        for (String s : subList) {
//            if (!shuffled.contains(s)) {
//                System.out.println(false);
//            }
//        }


        //drugie rozwiązanie
        Collections.sort(subList);
        Collections.sort(shuffled);

        System.out.println("13. " + subList.equals(shuffled));


        //trzecie rozwiązanie
        if (subList.size() != shuffled.size()) {
            System.out.println(false);
        }

        subList.removeAll(shuffled);

        if (subList.size() ==0) {
            System.out.println(true);
        }

//        14. Zamień miejscami dwa elementy w ArrayLiście (użyj klasy java.util.Collections).
        System.out.println();
        System.out.println("14. " + colors);

        Collections.swap(colors, 0, 1);

        System.out.println("14. " + colors);
        System.out.println();

//        15. Połącz dwie ArrayListy (addAll()).

        System.out.println();
        System.out.println("15. " + colors);

        colors.addAll(colors);

        System.out.println("15. " + colors);
        System.out.println();


//        16. Sklonuj ArrayListę (clone()).

        //metoda clone
        System.out.println();
        System.out.println("16. " + colors);

        ArrayList<String> clonedColors = (ArrayList<String>) ((ArrayList<String>) colors).clone();

        System.out.println("16. " + clonedColors);
        System.out.println();

//        17. Opróżnij ArrayListę (removeAll()).

        System.out.println();
        System.out.println("17. " + clonedColors);

        //pierwsze rozwiazanie
        clonedColors.clear();
        //drugie rozwiazanie
        clonedColors.removeAll(clonedColors);

        System.out.println("17. " + clonedColors);
        System.out.println();

//        18. Sprawdź, czy ArrayLista jest pusta (isEmpty()).

        System.out.println();
        System.out.println("18. " + clonedColors.isEmpty());
        System.out.println();

//        19. Przytnij ArrayListę do jej rozmiaru (trimToSize()).

        System.out.println("19.");
        //dlaczego potrzebne castowanie?
        ((ArrayList<String>) colors).trimToSize();

//        20. Zwiększ pojemność do wskazanej liczby (ensureCapacity()).

        System.out.println("20.");
        //dlaczego potrzebne castowanie?
        ((ArrayList<String>) colors).ensureCapacity(200);

//        21. Wypisz wszystkie elementy ArrayListy przy użyciu indeksu.

        System.out.println();
        for (int i = 0; i < colors.size(); i++) {
            System.out.println("21. " + colors.get(i));
        }
        System.out.println();

//---------------------------------------------------------------------------------LinkedList

        //        stworzenie pustej listy

        List<String> linkedList = new LinkedList<>();

//        sprawdzenie czy lista jest pusta

        linkedList.isEmpty();

//        sprawdzenie rozmiaru listy

        linkedList.size();

//        dodanie elementu do listy

        linkedList.add("blue");

//        na początku (prepend)

        ((LinkedList<String>) linkedList).addFirst("red");

//        na końcu (append)

        linkedList.add("red");
        ((LinkedList<String>) linkedList).addLast("blue");


//        w środku (insert)

        linkedList.add(1, "grey");

//                pobranie elementu z listy

        linkedList.get(1);

//        usunięcie elementu z listy

        linkedList.remove(1);
    }
}
