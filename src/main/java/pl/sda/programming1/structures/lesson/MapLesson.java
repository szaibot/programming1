package pl.sda.programming1.structures.lesson;

import java.util.*;

class MapLesson {

    public static void main(String[] args) {
        //        1.	Utwórz HashMapę i dodaj do niej kilka nazw kolorów.
//        Jako klucze przyjmij liczby od 1 do n.
//        Przeiteruj się po wpisach w mapie i wypisz
//        na konsolę ciąg znaków: klucz=wartość.


        Map<Integer, String> map = new HashMap<>();
        map.put(1, "blue");
        map.put(2, "red");
        map.put(3, "orange");

        for (Map.Entry<Integer, String> colour : map.entrySet()) {
            System.out.println(colour.getKey() + "-" + colour.getValue());
        }

//        2.	Sprawdź rozmiar mapy.

        System.out.println();
        System.out.println("2 " + map.size());
        System.out.println();

//        3.	Utwórz drugą HashMapę i dodaj wszystkie jej elementy do pierwszej.

        System.out.println();

        Map<Integer, String> newHashMap = new HashMap<>();
        newHashMap.put(2, "grey");
        newHashMap.put(5, "pink");
        newHashMap.put(null, "black");
        newHashMap.put(null, "white");
        newHashMap.put(6, null);
        newHashMap.put(7, null);

        System.out.println("3 " + map);
        map.putAll(newHashMap);
        System.out.println("3 " + map);

        System.out.println();

//        4.	Wyczyść HashMapę.

        System.out.println();
        System.out.println("4 " + newHashMap);
        newHashMap.clear();
        System.out.println("4 " + newHashMap);
        System.out.println();

//        5.	Sprawdź, czy HashMapa jest pusta.

        System.out.println();
        System.out.println("5 " + map.isEmpty());
        System.out.println("5 " + newHashMap.isEmpty());
        System.out.println();

//        6.	Sprawdź, czy HashMapa zawiera wpis o podanym kluczu.

        System.out.println();
        System.out.println("6 " + map.containsKey(3));
        System.out.println("6 " + map.containsKey(null));
        System.out.println("6 " + map.containsKey(30));
        System.out.println();


//        7.	Sprawdź, czy HashMapa zawiera wpis z podaną wartością.

        System.out.println();
        System.out.println("7 " + map);
        System.out.println("7 " + map.containsValue("blue"));
        System.out.println("7 " + map.containsValue(null));
        System.out.println("7 " + map.containsValue("brown"));
        System.out.println();

//        8.	Utwórz Set zawierający wszystkie wpisy w HashMapie.

        Set<Map.Entry<Integer, String>> entries = map.entrySet();

//        9.	Pobierz wartość z HashMapy poprzez klucz.

        System.out.println();
        System.out.println("9 " + map.get(1));
        System.out.println();

//        10.	Utwórz Set zawierający tylko klucze z HashMapy.

        System.out.println();
        System.out.println("10 " + map.keySet());
        System.out.println();


//        11.	Utwórz kolecję zawierający tylko wartości z HashMapy.


        ///////////////////A
        List<String> coloursList = new ArrayList<>();

        for (Map.Entry<Integer, String> colours : map.entrySet()) {
            coloursList.add(colours.getValue());
        }

        System.out.println();
        System.out.println("11a " + coloursList);
        System.out.println();

        ///////////////////B
        System.out.println();
        ArrayList<String> newColoursList = new ArrayList<>(map.values());
        System.out.println("11b " + newColoursList);
        System.out.println();
    }
}
