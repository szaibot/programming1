package pl.sda.programming1.structures.lesson;

import java.util.ArrayDeque;
import java.util.Queue;
import java.util.Stack;

class QueueStackLesson {
    public static void main(String[] args) {
        //        stos:
//        odłożenie elementu (push)
        System.out.println();
        Stack<String> stack = new Stack<>();

        System.out.println("1 "  + stack);
        stack.push("blue");
        stack.push("red");
        System.out.println("1 "  + stack);
        System.out.println();

//         zdjęcie elementu (pop)

        System.out.println();
        System.out.println("2 "  + stack);
        stack.pop();
        System.out.println("2 "  + stack);
        System.out.println();

//        podejrzenie wierzchniego elementu (peek)

        System.out.println();
        System.out.println("3 "  + stack);
        String peek = stack.peek();
        System.out.println("3 "  + stack);

        System.out.println("3 "  + peek);
        System.out.println();

//        sprawdzenie czy stos jest pusty

        System.out.println();
        System.out.println("4 "  + stack.isEmpty());
        System.out.println();




//        kolejka:
        Queue<String> queue = new ArrayDeque<>();

//        zakolejkowanie elementu (enqueue/offer)

        System.out.println();
        System.out.println("1. " + queue);
        queue.offer("blue");
        queue.offer("red");
        System.out.println("1. " + queue);
        System.out.println();


//        usunięcie elementu z kolejki (dequeue/poll)

        System.out.println();
        System.out.println("2. " + queue);
        queue.poll();
        System.out.println("2. " + queue);
        System.out.println();

//        podejrzenie elementu na początku kolejki (peek)

        System.out.println();
        System.out.println("3. " + queue);
        Object peek1 = queue.peek();
        System.out.println("3. " + queue);
        System.out.println("3. " + peek1);
        System.out.println();

//        sprawdzenie czy kolejka jest pusta

        System.out.println();
        System.out.println("4. " + queue.isEmpty());

    }
}
