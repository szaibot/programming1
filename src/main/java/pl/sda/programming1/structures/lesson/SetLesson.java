package pl.sda.programming1.structures.lesson;

import java.util.*;

class SetLesson {

    public static void main(String[] args) {
        //       1.	Utwórz HashSet<String> i dodaj do niego elementy będące
//       nazwami kolorów.

        System.out.println();
        Set<String> set = new HashSet<>();
        set.add("brown");
        set.add("black");
        System.out.println("1 " + set);
        System.out.println();

//        2.	Przeiteruj się po wszystkich elementach HashSetu. Użyj Iteratora.

        Iterator<String> iterator = set.iterator();
        while(iterator.hasNext()) {
            System.out.println("2 " + iterator.next());
        }

//        3.	Sprawdź ile elementów znajduje się w HashSecie.

        System.out.println();
        System.out.println("3 " + set.size());
        System.out.println();


//        4.	Opróżnij HashSet jedną metodą API.

        Set<String> newSet = new HashSet<>(set);

        System.out.println();
        System.out.println("4 " + set);
        set.clear();
        System.out.println("4 " + set);
        System.out.println();

//        5.	Sprawdź, czy HashSet jest pusty.

        System.out.println();
        System.out.println("5 " + set.isEmpty());
        System.out.println();

//        6.	Przekonwertuj HashSet do tablicy.

        System.out.println();
        System.out.println("6 " + newSet.toArray());
        System.out.println();

//        7.	Przekonwertuj HashSet do TreeSetu.

        System.out.println();
        TreeSet<String> treeSet = new TreeSet<>(newSet);
        System.out.println("7 " + treeSet);
        System.out.println();


//        8.	Przekonwertuj HashSet do ArrayListy.

        System.out.println();
        List<String> list = new ArrayList<>(newSet);
        System.out.println("8 " + list);
        System.out.println();

//        9.	Utwórz metodę porównującą dwa HashSety.

        System.out.println();
        //działa tylko na javie 9
        //System.out.println("9 " + compare(new HashSet<>(newSet), Set.of("brown", "black")));

        //rozwiązanie w javie 8
        Set<String> someSet = new HashSet<>();
        someSet.add("brown");
        someSet.add("black");
        System.out.println("9 " + compare(new HashSet<>(newSet), someSet));

        System.out.println();

//        10.	Utwórz drugi HashSet z elementami, które występują w pierwszym
//              (nie muszą być wszystkie),
//              oraz takimi, których tam nie ma.
//              Następnie usuń z pierwszego HashSetu
//              wszystkie kolory, których nie ma w drugim, przy użyciu jednej
//              metody API.

        System.out.println();
        Set<String> set2 = new HashSet<>();
        //newSet = "black"
        //newSet = "brown"

        set2.add("brown");
        set2.add("pink");

        System.out.println("10 " + newSet);
        System.out.println("10 " + set2);
        newSet.retainAll(set2);
        System.out.println("10 " + newSet);
        System.out.println("10 " + set2);
        System.out.println();


//        11.	Opróżnij HashSet jedną metodą API (inną niż w punkcie 4).

        System.out.println();
        System.out.println("11 " + newSet);
        newSet.removeAll(newSet);
        System.out.println("11 " + newSet);
        System.out.println();

    }

    static boolean compare(Set<String> first, Set<String> second) {
        if (first.size() != second.size()) {
            return false;
        }
        first.removeAll(second);
        return first.size() == 0;
    }
}
