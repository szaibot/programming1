package pl.sda.programming1.generics.live.examples;

import java.util.ArrayList;
import java.util.List;

/**
 * TODO
 */
class GenericCollections {
    public static void main(String[] args) {
        //runtime exception - run exmaple
        List list = new ArrayList();
        list.add("some string");
        list.add(1L);

        for (Object o : list) {
            System.out.println((String) o);
        }

        // UNCOMMENT - compile time error
//        List<String> stringList = new ArrayList<>();
//        stringList.add("some string");
//        stringList.add(1L);
    }
}
