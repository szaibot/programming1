package pl.sda.programming1.generics.answer.shoes;

/**
 * TODO
 */
public abstract class Shoes {
    //in kg
    private double weight;

    public Shoes(double weight) {
        this.weight = weight;
    }

    public double getWeight() {
        return weight;
    }
}
