package pl.sda.programming1.generics.answer;

import pl.sda.programming1.generics.answer.shoes.Shoes;

/**
 * TODO
 */
class Person<T extends Shoes> {
    private T shoes;

    public Person(T shoes) {
        this.shoes = shoes;
    }

    public T getShoes() {
        return shoes;
    }
}
