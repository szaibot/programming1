package pl.sda.programming1.generics.answer;

import pl.sda.programming1.generics.answer.shoes.Shoes;

import java.util.Comparator;

/**
 * TODO
 */
class WeightComparator implements Comparator<Shoes> {
    @Override
    public int compare(Shoes o1, Shoes o2) {
        if (o1.getWeight() - o2.getWeight() > 0) {
            return 1;
        }
        return -1;
    }
}
