package pl.sda.programming1.generics.answer.shoes;

/**
 * TODO
 */
public abstract class SportShoes extends Shoes {

    //from 1 to 10
    private int flexibility;

    public SportShoes(double weight, int flexibility) {
        super(weight);
    }

    public int getFlexibility() {
        return flexibility;
    }
}
