package pl.sda.programming1.generics.answer;

import pl.sda.programming1.generics.answer.shoes.TrekkingShoes;

/**
 * TODO
 */
class Trekker<T extends Clothes> extends Athlete<TrekkingShoes, T> {

    public Trekker(TrekkingShoes trekkingShoes, T clothes) {
        super(trekkingShoes, clothes);
        if (clothes.getMinDegree() > - 10) {
            throw new IllegalArgumentException("Wrong clothes for trekking!!");
        }
    }
}
