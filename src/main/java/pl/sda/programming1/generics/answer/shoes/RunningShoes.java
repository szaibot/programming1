package pl.sda.programming1.generics.answer.shoes;

/**
 * TODO
 */
public class RunningShoes extends SportShoes {

    //from 1 to 10
    private int amortizationLevel;

    public RunningShoes(double weight, int flexibility, int amortizationLevel) {
        super(weight, flexibility);
        this.amortizationLevel = amortizationLevel;
    }

    public int getAmortizationLevel() {
        return amortizationLevel;
    }
}
