package pl.sda.programming1.generics.answer;

import pl.sda.programming1.generics.answer.shoes.Hills;
import pl.sda.programming1.generics.answer.shoes.RunningShoes;
import pl.sda.programming1.generics.answer.shoes.Shoes;
import pl.sda.programming1.generics.answer.shoes.TrekkingShoes;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * TODO
 */
class Main {

    public static void main(String[] args) {
        Hills hills = new Hills(0.7, 15);
        Person<Hills> model = new Person<>(hills);
        RunningShoes runningShoes = new RunningShoes(0.3, 8, 5);
        Athlete<RunningShoes, Clothes> runner = new Athlete<>(runningShoes, new Clothes());
        TrekkingShoes trekkingShoes = new TrekkingShoes(2, 1, 8);
        Athlete<TrekkingShoes, Clothes> trekker = new Trekker<>(trekkingShoes, new Clothes());

        System.out.println("Model's shoes weigh " + model.getShoes().getWeight() + " kg");
        System.out.println("Runner's shoes weigh " + runner.getShoes().getWeight() + " kg");
        System.out.println("Trekker's shoes weigh " + trekker.getShoes().getWeight() + " kg");

        List<Shoes> shoes = Arrays.asList(trekkingShoes, runningShoes, hills);

        System.out.println("before");
        System.out.println(shoes);

        Collections.sort(shoes, new WeightComparator());

        System.out.println("after");
        System.out.println(shoes);
    }
}
