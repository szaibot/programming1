package pl.sda.programming1.generics.answer.shoes;

/**
 * TODO
 */
public class TrekkingShoes extends SportShoes {

    //from 1 to 10
    private int waterproofLevel;

    public TrekkingShoes(double weight, int flexibility, int waterproofLevel) {
        super(weight, flexibility);
        this.waterproofLevel = waterproofLevel;
    }

    public int getWaterproofLevel() {
        return waterproofLevel;
    }
}
