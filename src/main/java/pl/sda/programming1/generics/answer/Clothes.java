package pl.sda.programming1.generics.answer;

/**
 * TODO
 */
class Clothes {
    private int minDegree;
    private int maxDegree;

    public Clothes() {
        minDegree = -10;
        maxDegree = 20;
    }

    public int getMinDegree() {
        return minDegree;
    }

    public int getMaxDegree() {
        return maxDegree;
    }
}
