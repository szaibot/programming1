package pl.sda.programming1.generics.answer;

import pl.sda.programming1.generics.answer.shoes.SportShoes;

/**
 * TODO
 */
class Athlete<T extends SportShoes, R extends Clothes> extends Person<T> {
    private R clothes;

    public Athlete(T shoes, R clothes) {
        super(shoes);
        this.clothes = clothes;
    }

    public R getClothes() {
        return clothes;
    }
}