package pl.sda.programming1.repetition.inheritance.first;

abstract class Amphibian extends Animal {

    Amphibian(int age) {
        super(age, "gills, lungs and skin!");
    }
}