package pl.sda.programming1.repetition.inheritance.first;

abstract class Animal {
    private int age;
    private final String breathingOrgan;

    protected Animal(int age, String breathingOrgan) {
        this.age = age;
        this.breathingOrgan = breathingOrgan;
    }

    public void breathe() {
        System.out.println("I'm breathing with " + breathingOrgan + "!");
    }
}