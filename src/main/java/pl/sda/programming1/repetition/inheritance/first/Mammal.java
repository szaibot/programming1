package pl.sda.programming1.repetition.inheritance.first;

import static pl.sda.programming1.repetition.inheritance.first.Gender.MALE;

abstract class Mammal extends Animal {
    private final Gender gender;

    Mammal(int age, Gender gender) {
        super(age, "lungs");
        this.gender = gender;
    }

    public void feedWithBreast() {
        if (gender == MALE) {
            throw new IllegalArgumentException("CHŁOPY NIE KARMIĄ!!!");
        }

        System.out.println("I’m feeding with breast");
    }
}