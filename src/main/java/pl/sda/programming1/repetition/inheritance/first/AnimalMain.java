package pl.sda.programming1.repetition.inheritance.first;

import java.util.Arrays;
import java.util.List;

import static pl.sda.programming1.repetition.inheritance.first.Gender.FEMALE;
import static pl.sda.programming1.repetition.inheritance.first.Gender.MALE;

class AnimalMain {

    public static void main(String[] args) {
        //-----------------------------ZADANIE 6
        Animal lion = new Lion(3, FEMALE);
        Animal human = new Human(7, MALE);

        lion.breathe();
        human.breathe();

        //-----------------------------ZADANIE 7
        Animal frog = new Frog(1);
        frog.breathe();

        //-----------------------------ZADANIE 8
        Animal salmon = new Salmon(2);
        salmon.breathe();

        //-----------------------------ZADANIE 11
        List<Animal> animals = Arrays.asList(new Lion(3, FEMALE), new Human(7, FEMALE), new Frog(1), new Salmon(2));

        //wariant for
        for (Animal animal : animals) {
            animal.breathe();
        }

        //wariant forEach z lambdą
        animals.forEach(animal -> animal.breathe());

        //-----------------------------ZADANIE 16
        Mammal newLion = new Lion(10, FEMALE);
        newLion.feedWithBreast();

        Mammal zosia = new Human(25, FEMALE);
        zosia.feedWithBreast();


        //-----------------------------ZADANIE 17
        //Animal kingLion = new Lion(5);
        Mammal kingLion = new Lion(5, FEMALE);
        kingLion.feedWithBreast();

        //-----------------------------ZADANIE 19
        Mammal hubert = new Human(30, MALE);
        hubert.feedWithBreast();

    }
}
