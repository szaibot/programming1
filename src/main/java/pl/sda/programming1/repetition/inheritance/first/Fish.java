package pl.sda.programming1.repetition.inheritance.first;

abstract class Fish extends Animal {

    Fish(int age) {
        super(age, "gills");
    }
}