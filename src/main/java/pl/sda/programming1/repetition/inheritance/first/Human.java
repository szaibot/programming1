package pl.sda.programming1.repetition.inheritance.first;

class Human extends Mammal {
    Human(int age, Gender gender) {
        super(age, gender);
    }
}
