package pl.sda.programming1.repetition.inheritance.first;

class Lion extends Mammal {
    Lion(int age, Gender gender) {
        super(age, gender);
    }
}
