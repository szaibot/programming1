package pl.sda.programming1.repetition.inheritance.first;

class Frog extends Amphibian {
    Frog(int age) {
        super(age);
    }
}
