package pl.sda.programming1.lambda.example.first;

@FunctionalInterface
interface Holder<T> {
    T get();
}
