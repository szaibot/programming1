package pl.sda.programming1.lambda.example.second;

interface ParamStringHolder {

    String getString(String param);
}
