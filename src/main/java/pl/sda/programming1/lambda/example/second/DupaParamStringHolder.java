package pl.sda.programming1.lambda.example.second;

class DupaParamStringHolder implements ParamStringHolder {
    @Override
    public String getString(String param) {
        return param + "123";
    }
}
