package pl.sda.programming1.lambda.example.first;

import sun.net.www.content.text.Generic;

import java.util.function.Supplier;

/**
 * TODO
 */
class Main {

    public static void main(String[] args) {
        //DupaStringHolder to klasa która implementuje interfejs StringHolder.
        //Ten interfejs ma jedną metodę, która zwraca jakiś String.
        DupaStringHolder dupaStringHolder = new DupaStringHolder();
        String stringFromDupaStringHolder = dupaStringHolder.getString();
        System.out.println("String from DupaStringHolder:" + stringFromDupaStringHolder);

        //Inna implementacja interfejsu StringHolder
        HouseStringHolder houseStringHolder = new HouseStringHolder();
        String stringFromHouseStringHolder = houseStringHolder.getString();
        System.out.println("String from HouseStringHolder:" + stringFromHouseStringHolder);

        //Co tutaj nie jest sexy? To że za każdym razem musimy tworzyć nową klasę w innym pliku.
        //Odpowiedzią na to jest klasa anonimowa (implementacja iterfejsu tworzona w miejscu w którym inicjalizujemy obiekt tego interfejsu):
        //po lewej stronie musi być typ interfejsowy (klasa anonimowa nie ma swojego typu, w przeciwieństwie do innych klas (jak DupaStringHolder))
        StringHolder anonymousStringHolder = new StringHolder() {
            @Override
            public String getString() {
                return "DUPA";
            }
        };

        System.out.println("String from AnonymousStringHolder:" + anonymousStringHolder.getString());


        //problem? trochę dużo tych znaczków, idealnie chciałbym mieć tylko parametry i body metody.
        //I tutaj cała na biało wkracza lambda :)

                                        // po lewo od znaku -> mamy parametry, skopiowane z nagłówka metody klasy anoniomowej (tutaj nie mamy parametrów)
                                        // po prawo od znaku -> mamy całe
        StringHolder lambdaStringHolder = () -> {return "DUPA";};

        System.out.println("String from lambda StringHolder:" + lambdaStringHolder.getString());

        //pewne rzeczy da się jeszcze usunąć. Jeżeli lambda jest jednolinijkowa to można usunać klamry i słowo return :)
        StringHolder evenMoreLambdaStringHolder = () -> "DUPA";

        System.out.println("String from even more lambda StringHolder:" + evenMoreLambdaStringHolder.getString());



        //-----------------------------------------Generic Holder interface

        //w poprzednim przykładzie używaliśmy StringHoldera, który zwracał nam tylko Stringa, a co jeżeli chcielibyśmy stworzyć taki interfejs, który pasuje do różnych typów,
        // nie tylko dla Stringa. Odpowiedź to interfejs, ale z parametrem typu generycznego :)
        // Przykład poniżej:
        //Holder<String> zachowuje sie dokładnie tak jak StringHolder, jedynie typ zwracany przekazujemy przez parametr generyczny
        Holder<String> genericStringHolder = new DupaGenericStringHolder();
        System.out.println("String from generic holder" + genericStringHolder.get());

        //wersja z klasą anonimową
        Holder<String> anonymousGenericStringHolder = new Holder<String>() {
            @Override
            public String get() {
                return "DUPA";
            }
        };

        //wersja z lambdą
        Holder<String> lambdaGenericStringHolder = () -> "DUPA";
        System.out.println("String from lambda generic holder"+lambdaGenericStringHolder.get());

        //ale nie musi to być tylko String:
        Holder<Integer> lambdaGenericIntegerHolder = () -> 12345;
        System.out.println("Integer from lambda generic holder:" +lambdaGenericIntegerHolder.get());

        Holder<Double> lambdaGenericDoubleHolder = () -> 12.01;
        Holder<Boolean> lambdaGenericBooleanHolder = () -> false;

        //Interfejs Holder jest bardzo prosty, nie przyjmuje parametru, zwraca zmienną typu, który ustaliliśmy za pomocą parametru generycznego.
        //czyli Holder<String> nie przyjmuje nic i zwraca obiekt typu String, Holder<Vodka> nie przyjmuje nic i zwraca obiekt typu Vodka.

        //Java ma już taki interfejs, ten interfejs nazywa się Supplier i działa dokładnie tak jak interfejs Holder.
        //Więc nie musimy tworzyć swojego własnego, tylko użyć już istniejącego. Każdy go używa, używaj i ty:)
        // Przykład:

        //Przy użyciu holdera:
        Holder<Long> holderWithLong = () -> 12345678901L;

        //Przy użyciu suppliera:
        Supplier<Long> supplierWithLong = () -> 12345678901L;

        //wywołanie funkcji holdera
        Long holderLongValue = holderWithLong.get();
        System.out.println("Holder long value:" + holderLongValue);

        //wywołanie funkcji suppliera
        Long supplierLongValue = supplierWithLong.get();
        System.out.println("Supplier long value:" + holderLongValue);

        //zachęcam do przejrzenia klasy java.util.function.Supplier
    }
}
