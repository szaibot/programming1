package pl.sda.programming1.lambda.example.second;

@FunctionalInterface
interface GenericParamHolder<T, R> {
    R get(T t);
}
