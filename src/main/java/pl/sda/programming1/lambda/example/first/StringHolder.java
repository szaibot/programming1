package pl.sda.programming1.lambda.example.first;

//ta adnotacja powoduje, że nie możemy dodać drugiej metody abstrakcyjnej do interfejsu.
//Interfejsy funkcjyjne muszą mieć max 1 metodę abstrakcyjną (słówko abstract jest dodawanane do metody przez kompilator)
@FunctionalInterface
interface StringHolder {

    String getString();
}
