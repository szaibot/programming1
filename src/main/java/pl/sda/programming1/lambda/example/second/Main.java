package pl.sda.programming1.lambda.example.second;


import java.util.function.Function;

class Main {

    //parametrized lambda
    public static void main(String[] args) {
        ParamStringHolder paramStringHolder = new DupaParamStringHolder();
        String returnedString = paramStringHolder.getString("DUPA");
        System.out.println(returnedString);

        ParamStringHolder anonymousParamStringHolder = new ParamStringHolder() {
            @Override
            public String getString(String param) {
                return param + "1234";
            }
        };

        //to samo co wyżej tylko z a pomoca lambdy
        ParamStringHolder lambdaParamStringHolder = (String param) -> {return param + "1234";};

        //to samo co wyżej tylko, że bez typu parametru (jest on okreslony w interfejsie, więc jest tutaj zbędny)
        ParamStringHolder moreLambdaParamStringHolder = param -> param + "1234";

        //to samo co wyżej tylko nazwa zmiennej może być taka jaką tylko chcemy
        ParamStringHolder moreMoreLambdaParamStringHolder = p -> p + "1234";

        String dupa = moreLambdaParamStringHolder.getString("DUPA");
        System.out.println(dupa);

        //tak jak w przykładzie pierwszym z StringHolderem, tutaj tez chciałbym móc zmienać zwracany typ
        // (w ParamStringHolder to zawsze jest String), dodatkowo chciałbym zmieniać typ parametru, który dostarczam (tutaj też zawsze string)
        // Generyki jako odpowiedź, użyłem je w interfejsie GenericParamHolder
        //Przykład (takie samo wywołanie przy użyciu ParamStringHolder oraz GenericParamHolder:
        ParamStringHolder simpleParamStringHolder = param -> param + 123;
        String testest = simpleParamStringHolder.getString("testest");
        System.out.println(testest);

        GenericParamHolder<String, String> genericParamStringHolder = param -> param + 123;
        String drugi_test = genericParamStringHolder.get("drugi test");
        System.out.println(drugi_test);

        //ale możemy tez użyć tego do innych typów:

        //ta funkcja przyjmuje parametr i mnoży go przez 3 i zwraca wynik mnożenia jako int
        GenericParamHolder<Integer, Integer> nextHolder = intParam -> intParam * 3;
        Integer multiplyResult = nextHolder.get(10);
        System.out.println("Wynik mnozenia:" + multiplyResult);

        //przykład z innym typem
        GenericParamHolder<Double, Double> nextHolder2 = param -> param * 4.5;

        //typy mogą być tez inne, np. przyjmuje jako parametr string a zwracam jako int jego długość:
        GenericParamHolder<String, Integer> stringLenghtFunction = stringParam -> stringParam.length();
        Integer aaaaLength = stringLenghtFunction.get("aaaa");
        System.out.println("Lenght of String" + aaaaLength);

        //Tak jak w przypadku Holdera i Suppliera, tutaj też Java ma swój interfejs do tego. Jest to interfejs Function
        //Przykład:
        //przy użyciu GenericParamHolder:
        GenericParamHolder<Integer, Integer> genericParamHolder = integerParam -> integerParam * 2;
        Function<Integer, Integer> function = integerParam -> integerParam * 2;

        Integer integer = genericParamHolder.get(5);
        Integer integer2 = function.apply(5);//jedyne czym się różnią te interfejsy to nazwą metody
    }
}
