package pl.sda.programming1.lambda.exercise;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

public class LambdaExercise {

    private static final List<String> CITIES = Arrays.asList("Tokyo", "Rome", "New York", "Warsaw");

    public static void main(String[] args) {
        printCities();
    }

    /**
     * Wypisz wszystkie miasta z pola CITIES korzystając z metody forEach - {@link Iterable#forEach(Consumer)} oraz lambdy
     *
     * HINT: klasa List implementuje Iterable.
     */
    static void printCities() {
        //TODO implement
    }

    /**
     * Metoda ma za zadanie zwrócić funkcje, która sprawdza, czy liczba jest podzielna, przez wartość przekazaną przez parametr.
     */
    public Function<Integer, Boolean> isDivisibleBy(int number) {
        throw new UnsupportedOperationException("NOT IMPLEMENTED!!");
    }

    /**
     * Metoda ma za zadanie zwrócić Predykat, który sprawdza, czy przekazany przez parameter string jest taki sam jak dostarczony podczas wywołania.
     */
    public Predicate<String> isTheSame(String initString) {
        throw new UnsupportedOperationException("NOT IMPLEMENTED!!");
    }

    /**
     * Wywołaj metodę intComparatorInvoker, gdzie pierwszy parametr będzie lambdą implementującą interfejs MyIntComparator.
     * Lambda ta ma sprawdzać czy firstValue jest większy od secondValue
     */
    public boolean firstIsBigger(int firstValue, int secondValue) {
        throw new UnsupportedOperationException("NOT IMPLEMENTED!!");
    }


    protected boolean intComparatorInvoker(MyIntComparator myIntComparator, int firstValue, int secondValue) {
        return myIntComparator.compare(firstValue, secondValue);
    }

    protected interface MyIntComparator {
        boolean compare(int firstValue, int secondValue);
    }
}
