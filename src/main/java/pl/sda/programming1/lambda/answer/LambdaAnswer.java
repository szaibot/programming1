package pl.sda.programming1.lambda.answer;

import pl.sda.programming1.lambda.exercise.LambdaExercise;

import java.util.function.Function;
import java.util.function.Predicate;

public class LambdaAnswer extends LambdaExercise {

    @Override
    public Function<Integer, Boolean> isDivisibleBy(int number) {
        return x -> x % number == 0;
    }

    @Override
    public Predicate<String> isTheSame(String initString) {
        return providedString -> initString != null && initString.equals(providedString);
    }

    @Override
    public boolean firstIsBigger(int firstValue, int secondValue) {
        return intComparatorInvoker((first, second) -> first > second, firstValue, secondValue);
    }
}
